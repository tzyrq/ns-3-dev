/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Rui Zuo <ruitso2018@gmail.com>
 */

#ifndef REAL_SOCKET_HELPER_H 
#define REAL_SOCKET_HELPER_H

#include <future>
#include "ns3/real-socket.h"

namespace ns3 {

class RealSocketHelper {
public:
    RealSocketHelper();
    ~RealSocketHelper();
    void setPort();
    int getPort();
private:
    int port;
    RealSokcet rs;
};

} /* namespcae ns3 */

#endif /* REAL_SOCKET_HELPER_H */

