/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011-2018 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Rui Zuo
 */

#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/lte-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/geographic-positions.h"
#include "ns3/stats-module.h"

#include "ns3/spy-module.h"
#include "csv.h"

#include <fstream>
#include <iostream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("VERIFY-CELL");

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

std::ofstream ofSinr0;
std::ofstream ofSinr1;
std::ofstream ofSinr2;

void RsrpSinrTracedCallback0 (uint16_t cellId, uint16_t rnti, double rsrp, double sinr, uint8_t cqi, uint8_t componentCarrierId)  {
  ofSinr0 << Simulator::Now() << "," << cellId << "," << rnti << "," << rsrp << "," << sinr << "," << (uint32_t) cqi << endl;
}
void RsrpSinrTracedCallback1 (uint16_t cellId, uint16_t rnti, double rsrp, double sinr, uint8_t cqi, uint8_t componentCarrierId)  {
  ofSinr1 << Simulator::Now() << "," << cellId << "," << rnti << "," << rsrp << "," << sinr << "," << (uint32_t) cqi << endl;
}
void RsrpSinrTracedCallback2 (uint16_t cellId, uint16_t rnti, double rsrp, double sinr, uint8_t cqi, uint8_t componentCarrierId)  {
  ofSinr2 << Simulator::Now() << "," << cellId << "," << rnti << "," << rsrp << "," << sinr << "," << (uint32_t) cqi << endl;
}

int helper()
{
  unsigned int nBS = 2;
  unsigned int nUE = 3;
  ofSinr0 = std::ofstream ("SINR-0.csv");
  ofSinr1 = std::ofstream ("SINR-1.csv");
  ofSinr2 = std::ofstream ("SINR-2.csv");
  ofSinr0 << "Time stamp,Cell ID,RNTI,RSRP,SINR,CQI" << endl;
  ofSinr1 << "Time stamp,Cell ID,RNTI,RSRP,SINR,CQI" << endl;
  ofSinr2 << "Time stamp,Cell ID,RNTI,RSRP,SINR,CQI" << endl;

  Time simTime = Seconds (10);

  Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity" ,UintegerValue(320));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();

  // Set Handover Algorithm
  // Automatic handover
  // lteHelper->SetHandoverAlgorithmType ("ns3::A2A4RsrqHandoverAlgorithm");
  // lteHelper->SetHandoverAlgorithmAttribute ("ServingCellThreshold", UintegerValue (30));
  // lteHelper->SetHandoverAlgorithmAttribute ("NeighbourCellOffset", UintegerValue (1));
  
  // No handover
  lteHelper->SetHandoverAlgorithmType ("ns3::NoOpHandoverAlgorithm");

  uint8_t bandwidth = 100; // measured in number of RBs
  uint8_t num_RB = 16;
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetFfrAlgorithmType ("ns3::LteFrHardAlgorithm");
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));
  // Change the UP/DN link frequency to 2110MHz, 1920MH
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (0));
  lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (18000));

  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  // Install internet stack on remote host
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create internet between pgw and remote host
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (10)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  // interface 0 is localhost, 1 is the p2p device
  // Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  NodeContainer ueNodes;
  NodeContainer enbNodes;
  ueNodes.Create (nUE);
  enbNodes.Create (nBS);

  MobilityHelper bsMobility;
  bsMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  bsMobility.Install (enbNodes);
  bsMobility.Install (ueNodes);

  enbNodes.Get(0)->GetObject<MobilityModel> ()->SetPosition(
    Vector (0.0, 0.0, 0.0)
  );
  enbNodes.Get(1)->GetObject<MobilityModel> ()->SetPosition(
    Vector (2000.0, 0.0, 0.0)
  );

  ueNodes.Get(0)->GetObject<MobilityModel> ()->SetPosition (
    Vector (100.0, 0.0, 0.0)
  );
  ueNodes.Get(1)->GetObject<MobilityModel> ()->SetPosition (
    Vector (1900.0, 0.0, 0.0)
  );
  ueNodes.Get(2)->GetObject<MobilityModel> ()->SetPosition (
    Vector (1900.0, 0.0, 0.0)
  );

  // Install LTE devices on UEs and eNB
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  // Install IP stack on UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Assign IP address to UEs, and install applications
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
      Ptr<Node> ueNode = ueNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv4StaticRouting> ueStaticRouting =
          ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    }
  
  lteHelper->Attach(ueLteDevs.Get(0), enbLteDevs.Get(0));
  lteHelper->Attach(ueLteDevs.Get(1), enbLteDevs.Get(1));
  lteHelper->Attach(ueLteDevs.Get(2), enbLteDevs.Get(1));

  uint16_t dlPort = 1100;
  Time interval = MilliSeconds(10.0);
  Time startTimes = MilliSeconds(0.0);
  ApplicationContainer clientApps, serverApps;
  for (uint32_t i = 0; i <= 1; ++i) {
    ++dlPort;
    UdpClientHelper dlClientHelper (ueIpIface.GetAddress (i), dlPort);
    dlClientHelper.SetAttribute("Interval", TimeValue(interval));
    dlClientHelper.SetAttribute("StartTime", TimeValue(startTimes));
    clientApps.Add (dlClientHelper.Install (remoteHost));
    PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                         InetSocketAddress (Ipv4Address::GetAny (), dlPort));
    serverApps.Add (dlPacketSinkHelper.Install (ueNodes.Get(i)));
  }
  for (uint32_t i = 2; i <= 2; ++i) {
    ++dlPort;
    UdpClientHelper dlClientHelper (ueIpIface.GetAddress (i), dlPort);
    dlClientHelper.SetAttribute("Interval", TimeValue(interval));
    dlClientHelper.SetAttribute("StartTime", TimeValue(MilliSeconds(5)));
    clientApps.Add (dlClientHelper.Install (remoteHost));
    PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                         InetSocketAddress (Ipv4Address::GetAny (), dlPort));
    serverApps.Add (dlPacketSinkHelper.Install (ueNodes.Get(i)));
  }

  std::string nodeId = std::to_string(ueNodes.Get(0)->GetId());
  Config::ConnectWithoutContext ("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
    MakeCallback(&RsrpSinrTracedCallback0));
  nodeId = std::to_string(ueNodes.Get(1)->GetId());
  Config::ConnectWithoutContext ("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
    MakeCallback(&RsrpSinrTracedCallback1));
  nodeId = std::to_string(ueNodes.Get(2)->GetId());
  Config::ConnectWithoutContext ("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
    MakeCallback(&RsrpSinrTracedCallback2));
  
  Simulator::Stop (simTime);
  Simulator::Run ();

  Simulator::Destroy ();

  return 0;
}

int
main (int argc, char *argv[]) {
  helper();  
}