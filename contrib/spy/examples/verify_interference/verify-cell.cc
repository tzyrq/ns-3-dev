/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011-2018 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Rui Zuo
 */

#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/lte-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/geographic-positions.h"
#include "ns3/stats-module.h"

#include "ns3/spy-module.h"
#include "csv.h"

#include <fstream>
#include <iostream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("VERIFY-CELL");

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

std::ofstream ofTx;
std::ofstream ofRx;
std::ofstream ofSinr;

void ClientTxIMSITrace (Ptr<const Packet> pkt, Time timegap, uint32_t ByteSum, uint32_t tx_id, uint32_t rx_id) {
  ofTx << Simulator::Now() << "," << tx_id << "," << rx_id << "," << ByteSum * 8.0 / timegap.GetSeconds () / 1000 / 1000 << endl;
}

void ClientRxIMSITrace(Ptr<const Packet> pkt, Time timegap, uint32_t ByteSum, uint32_t tx_id, uint32_t rx_id){
  ofRx << Simulator::Now() << "," << tx_id << "," << rx_id << "," << ByteSum * 8.0 / timegap.GetSeconds () / 1000 / 1000 << endl;
}

void RsrpSinrTracedCallback (uint16_t cellId, uint16_t rnti, double rsrp, double sinr, uint8_t cqi, uint8_t componentCarrierId)  {
  ofSinr << Simulator::Now() << "," << cellId << "," << rnti << "," << rsrp << "," << sinr << "," << (uint32_t) cqi << endl;
}

int helper(int nBS, double enbTxPower)
{
  std::string prefix = std::to_string(nBS) + "BS-" + std::to_string((int) enbTxPower) + "EnbTxPower" ;
  ofTx = std::ofstream (prefix + "-verify-cell-Tx.csv");
  ofRx = std::ofstream (prefix + "-verify-cell-Rx.csv");
  ofSinr = std::ofstream (prefix + "-verify-cell-SINR.csv");
  ofTx << "Time stamp,Client IMSI,Sever IMSI,Tx Throughput (Mbps)" << endl;
  ofRx << "Time stamp,Client IMSI,Sever IMSI,Rx Throughput (Mbps)" << endl;
  ofSinr << "Time stamp,Cell ID,RNTI,RSRP,SINR,CQI" << endl;

  Time simTime = Seconds (10);
  bool disableDl = false, disableUl = false;

  int nUE = 10 + nBS - 1;
  Time windowSize =  MilliSeconds (1000);
  Time interPacketInterval = MilliSeconds (100);

  Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity" ,UintegerValue(320));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();

  // Set Handover Algorithm
  // Automatic handover
  // lteHelper->SetHandoverAlgorithmType ("ns3::A2A4RsrqHandoverAlgorithm");
  // lteHelper->SetHandoverAlgorithmAttribute ("ServingCellThreshold", UintegerValue (30));
  // lteHelper->SetHandoverAlgorithmAttribute ("NeighbourCellOffset", UintegerValue (1));
  
  // No handover
  lteHelper->SetHandoverAlgorithmType ("ns3::NoOpHandoverAlgorithm");

  uint8_t bandwidth = 100; // measured in number of RBs
  uint8_t num_RB = 16;
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetFfrAlgorithmType ("ns3::LteFrHardAlgorithm");
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));
  // Change the UP/DN link frequency to 2110MHz, 1920MH
  // lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (0));
  // lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (18000));

  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  // Install internet stack on remote host
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create internet between pgw and remote host
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (10)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  // interface 0 is localhost, 1 is the p2p device
  Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  NodeContainer ueNodes;
  NodeContainer enbNodes;
  ueNodes.Create (nUE);
  enbNodes.Create (nBS);

  // Install Mobility for BS
  //Ptr<ListPositionAllocator> bsPositionAlloc = CreateObject<ListPositionAllocator> ();
  //for (auto &e : bsCoor)
  //  {
  //    std::string lat = e.second.first, lng = e.second.second;
  //    Vector bsPosition = GeographicPositions::GeographicToCartesianCoordinates (
  //        std::stod (lat), std::stod (lng), 100.0, GeographicPositions::SPHERE);
  //    bsPositionAlloc->Add (bsPosition);
  //  }

  std::vector<vector<double>> bsCoor {
    // UEs connected attached to this cell
    {43.037605106651895, -76.13405583723511}, 
    {43.03763122056822, -76.12654814059889},
    {43.03762877328916, -76.12730485261203},
    {43.037623878730734, -76.12804482320895},
    {43.0376189841717, -76.12880153524038},
    {43.03761776053193, -76.1295482024038},
    {43.03761286597252, -76.13029486959007},
    {43.03760552413319, -76.13104321090209},
    {43.03760062957311, -76.13179824878422},
    {43.03759940593337, -76.13254659010144},
    {43.03759451137277, -76.13329660555753},
  };

  MobilityHelper bsMobility;
  bsMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  //bsMobility.SetPositionAllocator (bsPositionAlloc);
  bsMobility.Install (enbNodes);
  // NS_LOG_INFO ("eNB at " << enbNodes.Get(0)->GetObject<MobilityModel>() ->GetPosition() << "\n");

  for (int i = 0; i < nBS; i++)
  {
    const vector<double> &coor = bsCoor[i];
    double lat = coor[0], lng = coor[1];
    Vector bsPosition = GeographicPositions::GeographicToCartesianCoordinates (
        lat, lng, 100.0, GeographicPositions::SPHERE);
    Ptr<MobilityModel> enbMob = enbNodes.Get(i)->GetObject<MobilityModel> ();
    enbMob->SetPosition (bsPosition);
  }

  std::vector<std::vector<double>> ueCoor {
    {43.042, -76.1315},
    {43.0335, -76.1382},
    {43.0423, -76.1323},
    {43.0353, -76.1381},
    {43.0397, -76.1308},
    {43.0347, -76.1376},
    {43.0401, -76.131},
    {43.0328, -76.1387},
    {43.0411, -76.1302},
    {43.036, -76.1375},
  };

  // Install mobility for ue 
  MobilityHelper ueMobility;
  ueMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  ueMobility.Install (ueNodes);
  for (uint32_t i = 0; i < 10; i++) {
    Ptr<MobilityModel> ueMob = ueNodes.Get(i)->GetObject<MobilityModel> ();
    double lat = ueCoor[i][0];
    double lng = ueCoor[i][1];
    Vector uePosition = GeographicPositions::GeographicToCartesianCoordinates (
      lat, lng, 100.0, GeographicPositions::SPHERE);
    ueMob->SetPosition (uePosition);
  }
  for (uint32_t i = 10; i < ueNodes.GetN(); i++) {
    Ptr<MobilityModel> ueMob = ueNodes.Get(i)->GetObject<MobilityModel> ();
    double bsLat = bsCoor[i-10+1][0], bsLng = bsCoor[i-10+1][1];
    // double lat = bsLat + std::pow((double) -1, (double) i) * fRand(0.001, 0.005);
    // double lng = bsLng + std::pow((double) -1, (double) i) * fRand(0.001, 0.005);
    double lat = bsLat + 0.005;
    double lng = bsLng + 0.005;
    Vector uePosition = GeographicPositions::GeographicToCartesianCoordinates (
      lat, lng, 100.0, GeographicPositions::SPHERE);
    ueMob->SetPosition (uePosition);
  }

  // Install LTE devices on UEs and eNB
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  for (uint32_t i = 1; i < enbNodes.GetN(); ++i) {
    std::string nodeId = std::to_string(enbNodes.Get(i)->GetId());
    Config::Set("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteEnbNetDevice/ComponentCarrierMap/*/LteEnbPhy/TxPower", DoubleValue(enbTxPower));
  }

  // Install IP stack on UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Assign IP address to UEs, and install applications
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
      Ptr<Node> ueNode = ueNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv4StaticRouting> ueStaticRouting =
          ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    }
  
  // attach all UE to first eNB
  // lteHelper->Attach(ueLteDevs, enbLteDevs.Get(0));

  // Attach 1-10 to first eNB, last Ue to second eNB
  for (uint32_t i = 0; i < 10; i++) {
    lteHelper->Attach(ueNodes.Get(i)->GetDevice(0), enbNodes.Get(0)->GetDevice(0));
  }
  for (uint32_t i = 10; i < ueNodes.GetN(); i++) {
    lteHelper->Attach(ueLteDevs.Get(i), enbLteDevs.Get(i - 10 + 1));
  }


  // Install and start applications on UEs and remote host
  uint16_t dlPort = 1100;
  uint16_t ulPort = 2000;
  ApplicationContainer dlClientApps, dlServerApps;
  ApplicationContainer ulClientApps, ulServerApps;
  uint32_t packetSize = 500;
  uint32_t maxPackets = 4294967295;
  for (uint32_t i = 0; i < ueNodes.GetN (); ++i)
    {
      if (!disableDl)
        { ++dlPort;
          UdpEchoServerHelper dlServer (dlPort);
          dlServer.SetAttribute ("CIMSI", UintegerValue (10000)); // 10000 for remoteHost
          dlServer.SetAttribute ("SIMSI", UintegerValue (ueNodes.Get(i)->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi()));
          dlServerApps.Add (dlServer.Install (ueNodes.Get (i)));

          UdpEchoClientHelper dlClient (ueIpIface.GetAddress (i), dlPort);
          dlClient.SetAttribute ("Interval", TimeValue (interPacketInterval));
          dlClient.SetAttribute ("LocalAddress", AddressValue (ueIpIface.GetAddress (i)));
          dlClient.SetAttribute ("windowSize", TimeValue (windowSize));
          dlClient.SetAttribute ("CIMSI", UintegerValue (10000));
          dlClient.SetAttribute ("SIMSI", UintegerValue (ueNodes.Get(i)->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi()));
          dlClient.SetAttribute ("MaxPackets", UintegerValue (maxPackets));
          dlClient.SetAttribute ("PacketSize", UintegerValue (packetSize));
          dlClientApps.Add (dlClient.Install (remoteHost));
        }
      if (!disableUl)
        {
          ++ulPort;
          UdpEchoServerHelper ulServer (ulPort);
          ulServer.SetAttribute ("CIMSI", UintegerValue (ueNodes.Get(i)->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi()));
          ulServer.SetAttribute ("SIMSI", UintegerValue (10000));
          ulServerApps.Add (ulServer.Install (remoteHost));

          UdpEchoClientHelper ulClient (remoteHostAddr, ulPort);
          ulClient.SetAttribute ("Interval", TimeValue (interPacketInterval));
          ulClient.SetAttribute ("windowSize", TimeValue (windowSize));
          ulClient.SetAttribute ("MaxPackets", UintegerValue (maxPackets));
          ulClient.SetAttribute ("CIMSI", UintegerValue (ueNodes.Get(i)->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi()));
          ulClient.SetAttribute ("SIMSI", UintegerValue (10000));
          ulClient.SetAttribute ("PacketSize", UintegerValue (packetSize));
          ulClientApps.Add (ulClient.Install (ueNodes.Get(i)));
        }
    }
  
  // UE trace sinks
  for (uint32_t i = 0; i < 10; i++) {
    std::string nodeId = std::to_string(ueNodes.Get(i)->GetId());
    Config::ConnectWithoutContext("/NodeList/" + nodeId + "/ApplicationList/*/$ns3::UdpEchoClient/TxIMSI",
      MakeCallback(&ClientTxIMSITrace));
    Config::ConnectWithoutContext("/NodeList/" + nodeId + "/ApplicationList/*/$ns3::UdpEchoClient/RxIMSI",
      MakeCallback(&ClientRxIMSITrace));
    Config::ConnectWithoutContext ("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
      MakeCallback(&RsrpSinrTracedCallback));
  }
  

  // Set start time for UE
  // dlServerApps.Start (MilliSeconds (200));
  // ulClientApps.Start (MilliSeconds (200));
  // dlServerApps.Stop (simTime);
  // ulClientApps.Stop (simTime);

  // Set start time for eNB to start immediately
  // dlClientApps.Start (MilliSeconds (100));
  // ulServerApps.Start (MilliSeconds (100));
  // dlClientApps.Stop (simTime);
  // ulServerApps.Stop (simTime);
   
  //lteHelper->EnableTraces ();

  Simulator::Stop (simTime);
  Simulator::Run ();

  Simulator::Destroy ();

  return 0;
}

int
main (int argc, char *argv[])
{
  for (double power = 30; power <= 50; power += 5) {
    for (int i = 1; i <= 11; i++) {
      helper(i, power);
    }
  }
}
