/*
 * Test waypoint mobility model
 */

#include <iostream>
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/internet-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("TestWaypoint");

int 
main(int argc, char *argv[])
{
    NodeContainer nodes;
    nodes.Create (1);

    MobilityHelper nodesMobility;
    nodesMobility.SetMobilityModel ("ns3::WaypointMobilityModel");
    nodesMobility.Install (nodes);

    Ptr<Node> node = nodes.Get (0);
    Ptr<MobilityModel> mm = node->GetObject<MobilityModel> ();
    if (mm) std::cout << "Success\n"; 
    Ptr<WaypointMobilityModel> wmm = DynamicCast<WaypointMobilityModel> (mm);
    std::cout << wmm->GetPosition() << std::endl;
    return 0;
}
