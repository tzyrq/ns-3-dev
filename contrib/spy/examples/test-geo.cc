#include <iostream>
#include "ns3/geographic-positions.h"
#include "ns3/core-module.h"

int
main (int argc, char *argv[]) 
{
    ns3::Vector  vec {100, 100, 6378000};
    auto lla = ns3::GeographicPositions::CartesianToGeographicCoordinates(vec, ns3::GeographicPositions::SPHERE);
    // std::cout << "x = " << lla.x << "y = " << lla.y << "z = " << lla.z << std::endl;
    std::cout << lla << std::endl;
    auto pos = ns3::GeographicPositions::GeographicToCartesianCoordinates(
        0.0, 
        0.0,
        100.0,
        ns3::GeographicPositions::SPHERE);
    std::cout << pos << std::endl;
    pos = ns3::GeographicPositions::GeographicToCartesianCoordinates(
        43.0481,
        76.1474,
        0.0,
        ns3::GeographicPositions::SPHERE
    );
    std::cout << pos << std::endl;
    pos = ns3::GeographicPositions::GeographicToCartesianCoordinates(
        43.0481,
        76.1474,
        100.0,
        ns3::GeographicPositions::SPHERE
    );
    std::cout << pos << std::endl;

    return 0;
}