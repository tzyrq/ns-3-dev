#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/lte-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/geographic-positions.h"
#include "ns3/stats-module.h"

#include <iostream>
#include <thread>
#include <random>
#include <unistd.h>
#include <sys/wait.h>
#include <cmath>

#include <boost/math/constants/constants.hpp>
#include <boost/filesystem.hpp>

using namespace std;
using namespace ns3;

const double r = 2000.0;
const double R = 8000.0;

uint64_t idx = 0;
const double pi = boost::math::constants::pi<double>();

random_device rdDev;
uint32_t rdSeed = 100;
mt19937 gen(rdDev());

std::unordered_map<uint32_t, uint64_t> rntiToImsi;
std::ofstream ofSinr;

uint16_t subject_bs_cellid;
uint8_t number_of_ue_attached_to_subject_bs = 0;

class Paras {
public:
  Paras(unsigned int number_of_intf_ue, double ontime_mean, double offtime_mean):
    number_of_intf_ue(number_of_intf_ue), ontime_mean(ontime_mean), offtime_mean(offtime_mean)
  {}
  unsigned int number_of_intf_ue;
  double ontime_mean;
  double offtime_mean;
};

int randomInt(int left, int right) {
  static bool isSown = false;
  if (!isSown) {
    gen.seed(rdSeed);
    isSown = true;
  }
  uniform_int_distribution<int> dist(left, right);
  return dist(rdDev);
}

double randomDouble (double left, double right) {
  static bool isSown = false;
  if (!isSown) {
    gen.seed(rdSeed);
    isSown = true;
  }
  uniform_real_distribution<double> dist(left, right);
  return dist(rdDev);
}

bool randomBool() {
  double a = randomDouble(-1.0, 1.0);
  if (a > 0) return true;
  else return false;
}

uint32_t combineCellIdRNTI(uint16_t cellId, uint16_t rnti) {
    uint32_t res = (uint32_t) cellId;
    res = (res << 16) + rnti;
    return res;
}

void RsrpSinrTracedCallback (uint16_t cellId, uint16_t rnti, double rsrp, double sinr, uint8_t cqi, uint8_t componentCarrierId)  {
  try {
    uint64_t imsi = rntiToImsi.at(combineCellIdRNTI(cellId, rnti));
    ofSinr.precision(17);
    ofSinr << Simulator::Now() << "," << imsi << "," << cellId << "," << rnti << "," << rsrp << "," << sinr << "," << (uint32_t) cqi << endl;
  } catch (std::out_of_range& e) {}
}

void NotifyConnectionEstablishedUe (std::string context,
  uint64_t imsi, uint16_t cellid, uint16_t rnti) {
  cout << "[NotifyConnectionEstablishedUe]" << "imsi: " << imsi << " cell id: " << cellid << " rnti: " << rnti << endl;
  if (cellid == subject_bs_cellid) number_of_ue_attached_to_subject_bs++;
  rntiToImsi.emplace(combineCellIdRNTI(cellid, rnti), imsi);
  assert(number_of_ue_attached_to_subject_bs <= 1);
}

int runDL(const Paras &paras) {
  const unsigned int num_intfBS = 10;
  const unsigned int num_intfUe = paras.number_of_intf_ue;

  string prefix = "/home/rzuo02/work/ns-3-dev/model-data/";
  if (!boost::filesystem::exists(prefix))
    boost::filesystem::create_directory(prefix);

  ofstream ofParas(prefix + "paras_" + to_string(idx) + ".csv");
  ofParas << "Number of Interfering Ue,On Time Mean,Off Time Mean" << endl;
  ofParas << to_string(paras.number_of_intf_ue) << ","
    << to_string(paras.ontime_mean) << "," << to_string(paras.offtime_mean) << endl;

  rntiToImsi = {};
  ofSinr = std::ofstream (prefix + to_string(idx) + "-DL-SINR.csv");
  ofSinr << "Time stamp,IMSI,Cell ID,RNTI,RSRP,SINR,CQI" << endl;

  ++idx;
  Time simTime = Seconds (1);

  Config::SetDefault("ns3::LteEnbRrc::AdmitHandoverRequest", BooleanValue(false));
  Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity" ,UintegerValue(320));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));
  // Switch for DL SINR method
  // Config::SetDefault ("ns3::LteHelper::UsePdschForCqiGeneration", BooleanValue(false));

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();

  uint8_t bandwidth = 100; // measured in number of RBs
  uint8_t num_RB = 16;
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetFfrAlgorithmType ("ns3::LteFrHardAlgorithm");
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));

  // Change the DN link frequency to 2110MHz, UL to 1920MH
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (0));
  lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (18000));

  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  // Install internet stack on remote host
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create internet between pgw and remote host
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (10)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  // in this container, interface 0 is the pgw, 1 is the remoteHost
  // Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  NodeContainer enbNodes;
  enbNodes.Create (num_intfBS + 1);
  NodeContainer ueNodes;
  ueNodes.Create(paras.number_of_intf_ue + 1);

  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (enbNodes);
  mobility.Install (ueNodes);

  ueNodes.Get(0)->GetObject<MobilityModel> ()->SetPosition (
    Vector (0.0, 300.0, 0.0)
  );
  enbNodes.Get(0)->GetObject<MobilityModel> ()->SetPosition (
    Vector (0.0, 0.0, 0.0)
  );

  const double unit_arc = 1.0 / (double) num_intfBS;
  for (uint32_t i = 0; i < num_intfBS; ++i) {
    double units = i;
    double arc = units * unit_arc * 2.0 * pi;
    double x = R * std::sin (arc);
    double y = R * std::cos (arc);
    enbNodes.Get(i+1)->GetObject<MobilityModel> ()->SetPosition (
      Vector (x, y, 0.0)
    );
  }

  for (uint32_t i = 1; i < num_intfUe+1; ++i) {
    double xsign = randomBool() ? 1.0 : -1.0;
    bool ysign = randomBool() ? 1.0 : -1.0;
    double x = xsign * (r + randomDouble(r, R));
    double y = ysign * (r + randomDouble(r, R));
    ueNodes.Get(i)->GetObject<MobilityModel> ()->SetPosition(
      Vector(x, y, 100.0)
    );
  }

  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  enbNodes.Get (0)->GetDevice (0)->GetObject<LteEnbNetDevice> ()->GetRrc ()->SetCsgId (0, true);
  ueNodes.Get (0)->GetDevice (0)->GetObject<LteUeNetDevice> ()->SetCsgId(0);

  // Install IP stack on UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Assign IP address to UEs, and install applications
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u) {
    Ptr<Node> ueNode = ueNodes.Get (u);
    // Set the default gateway for the UE
    Ptr<Ipv4StaticRouting> ueStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
    ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
  }

  lteHelper->Attach (ueLteDevs.Get(0), enbLteDevs.Get(0));
  for (uint8_t i = 1; i < num_intfUe+1; ++i) {
    lteHelper->AttachToClosestEnb(ueLteDevs.Get(i), enbLteDevs);
  }

  // Install and start applications on UEs and remote host
  uint16_t dlPort = 1100;
  uint32_t packetSize = 500;
  string dataRate = "100Gb/s";
  ApplicationContainer clientApps, serverApps;
  for (uint32_t i = 0; i < num_intfUe+1; ++i) {
    ++dlPort;
    OnOffHelper DlOnOff("ns3::UdpSocketFactory", Address(InetSocketAddress(ueIpIface.GetAddress (i), dlPort)));
    DlOnOff.SetAttribute ("OnTime", StringValue ("ns3::ExponentialRandomVariable[Mean=" + to_string(paras.ontime_mean) + "]")); // unit is second
    DlOnOff.SetAttribute ("OffTime",StringValue ("ns3::ExponentialRandomVariable[Mean=" + to_string(paras.offtime_mean) + "]"));
    DlOnOff.SetAttribute ("PacketSize", UintegerValue (packetSize));
    DlOnOff.SetAttribute ("DataRate", DataRateValue(DataRate (dataRate)));
    DlOnOff.SetAttribute ("MaxBytes", UintegerValue (0));
    clientApps.Add (DlOnOff.Install (remoteHost));
    PacketSinkHelper DlSink("ns3::UdpSocketFactory",InetSocketAddress(ueIpIface.GetAddress (i),dlPort));
    serverApps.Add (DlSink.Install (ueNodes.Get (i)));
  }

  subject_bs_cellid = enbNodes.Get(0)->GetDevice(0)->GetObject<LteEnbNetDevice>()->GetCellId();
  std::string nodeId = std::to_string(ueNodes.Get(0)->GetId());
  Config::ConnectWithoutContext ("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
    MakeCallback(&RsrpSinrTracedCallback));
  Config::Connect ("/NodeList/" + nodeId + "/DeviceList/*/LteUeRrc/ConnectionEstablished",
    MakeCallback (&NotifyConnectionEstablishedUe));

  Simulator::Stop (simTime);

  Simulator::Run ();

  Simulator::Destroy ();

  return 0;
}

Paras gen_para() {
  unsigned int number_of_intf_ue = randomInt(1, 254);
  double ontime_mean = randomDouble(0.0, 1.0);
  double offtime_mean = randomDouble(0.0, 1.0);
  return Paras(number_of_intf_ue, ontime_mean, offtime_mean);
}

int main(int argc, char const *argv[]) {
  // pid_t ppid = getpid();
  // cout << ppid << endl;
  // const unsigned int cores = std::thread::hardware_concurrency();
  // const unsigned int N = 20000;
  // const int max_uav = 254;
  // const unsigned int workload = (N + cores - 1) / cores; // rounding up
  // for (unsigned int i = 0; i < cores; ++i) {
  //   pid_t pid = fork();
  //   unsigned int offset = workload * i;
  //   if (pid == 0) {
  //     for (unsigned int j = 1; j <= workload; ++j) {
  //       if (offset + j > N) break;
  //       unsigned int n_uav = (unsigned int) randomInt(1, max_uav);
  //       runDL(offset + j, n_uav);
  //     }
  //     return 0;
  //   }
  //   else {
  //     cout << "Core " << i << " " << offset << ", " << offset + workload << endl;
  //   }
  // }
  // while(wait(NULL) > 0); // parent process wait for all of child process to terminate.
  //When there is no child process, error code -1 raise and end while loop
  Paras paras(254, 0.5, 0.5);
  runDL(paras);
  return 0;
}
