#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/lte-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/geographic-positions.h"
#include "ns3/stats-module.h"

#include <iostream>
#include <thread>
#include <random>
#include <unistd.h>
#include <sys/wait.h>
#include <cmath>

#include <boost/math/constants/constants.hpp>
#include <boost/filesystem.hpp>

using namespace std;
using namespace ns3;

uint32_t idx = 0;
const double radius = 1000.0;

uint64_t seq_number;
bool subj_switch;
const double pi = boost::math::constants::pi<double>();

random_device rdDev;
// uint32_t rdSeed = 100;
// mt19937 gen(rdDev());

std::ofstream ofSubjectApp;
std::ofstream ofIntfApp;

int randomInt(int left, int right) {
  // static bool isSown = false;
  // if (!isSown) {
    // gen.seed(rdSeed);
    // isSown = true;
  // }
  uniform_int_distribution<int> dist(left, right);
  return dist(rdDev);
}

double randomDouble (double left, double right) {
  // static bool isSown = false;
  // if (!isSown) {
    // gen.seed(rdSeed);
    // isSown = true;
  // }
  uniform_real_distribution<double> dist(left, right);
  return dist(rdDev);
}

uint32_t combineCellIdRNTI(uint16_t cellId, uint16_t rnti) {
    uint32_t res = (uint32_t) cellId;
    res = (res << 16) + rnti;
    return res;
}

void subjTxTrace(std::string context, Ptr<const Packet> pkt, const Address& tx, const Address& rx){
  subj_switch = true;
  try {
    ofSubjectApp << seq_number++ << ","
      << Simulator::Now() << ","
      << InetSocketAddress::ConvertFrom(tx).GetIpv4() << ","
      << InetSocketAddress::ConvertFrom(rx).GetIpv4() << endl;
  } catch (std::out_of_range& e) {}
}

void subjRxTrace(std::string context, Ptr<const Packet> pkt, const Address& tx, const Address& rx){
  subj_switch = false;
  try {
    ofSubjectApp << "Rx " << seq_number-1 << ","
      << Simulator::Now() << ","
      << InetSocketAddress::ConvertFrom(tx).GetIpv4() << ","
      << InetSocketAddress::ConvertFrom(rx).GetIpv4() << endl;
  } catch (std::out_of_range& e) {}
}

void intfTxTrace(std::string context, Ptr<const Packet> pkt, const Address& tx, const Address& rx){
  if (!subj_switch) return;
  try {
    ofIntfApp << seq_number << ","
      << Simulator::Now() << ","
      << InetSocketAddress::ConvertFrom(tx).GetIpv4() << ","
      << InetSocketAddress::ConvertFrom(rx).GetIpv4() << endl;
  } catch (std::out_of_range& e) {}
}

class Paras {
public:
  Paras(unsigned int number_of_subj, unsigned int number_of_intf_ue):
    number_of_intf_ue(number_of_intf_ue),
    number_of_subj(number_of_subj)
  {}
  unsigned int number_of_intf_ue;
  unsigned int number_of_subj;
};

int runDL(const Paras &paras) {
  subj_switch = false;
  seq_number = 0;
  const int numBS = 2;
  
  string prefix = "/home/rzuo02/work/ns-3-dev/model-data/";
  if (!boost::filesystem::exists(prefix))
    boost::filesystem::create_directory(prefix);

  ofSubjectApp = std::ofstream (prefix + to_string(idx) + "-" + to_string(paras.number_of_subj) + "-subj.csv");
  ofSubjectApp << "Seq number,Time stamp,From,To" << endl;
  ofIntfApp = std::ofstream (prefix + to_string(idx) + "-" + to_string(paras.number_of_intf_ue) + "-intf.csv");
  ofIntfApp << "Seq number,Time stamp,From,To" << endl;

  idx++;
  Time simTime = Seconds (5);

  Config::SetDefault("ns3::LteEnbRrc::AdmitHandoverRequest", BooleanValue(false));
  Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity" ,UintegerValue(320));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));
  // Switch for DL SINR method
  // Config::SetDefault ("ns3::LteHelper::UsePdschForCqiGeneration", BooleanValue(false));

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();

  uint8_t bandwidth = 100; // measured in number of RBs
  uint8_t num_RB = 16;
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetFfrAlgorithmType ("ns3::LteFrHardAlgorithm");
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));

  // Change the DN link frequency to 2110MHz, UL to 1920MH
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (0));
  lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (18000));

  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  // Install internet stack on remote host
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create internet between pgw and remote host
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (10)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  // in this container, interface 0 is the pgw, 1 is the remoteHost
  // Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  NodeContainer enbNodes;
  enbNodes.Create (numBS);
  NodeContainer ueNodes;
  ueNodes.Create(paras.number_of_subj + paras.number_of_intf_ue);

  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (enbNodes);
  mobility.Install (ueNodes);

  enbNodes.Get(0)->GetObject<MobilityModel> ()->SetPosition (
    Vector (0.0, 0.0, 0.0)
  );
  enbNodes.Get(1)->GetObject<MobilityModel> ()->SetPosition (
    Vector (3000.0, 0.0, 0.0)
  );

  for (uint32_t i = 0; i < paras.number_of_subj; ++i) {
    double x = randomDouble(-radius, radius);
    double y = randomDouble(-radius, radius);
    ueNodes.Get(i)->GetObject<MobilityModel> ()->SetPosition(
      Vector(x, y, 100.0)
    );
  }
  for (uint32_t i = 0; i < paras.number_of_intf_ue; ++i) {
    double x = 3000.0 + randomDouble(-radius, radius);
    double y = randomDouble(-radius, radius);
    ueNodes.Get(i+paras.number_of_subj)->GetObject<MobilityModel> ()->SetPosition(
      Vector(x, y, 100.0)
    );
  }

  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  // Install IP stack on UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Assign IP address to UEs, and install applications
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u) {
    Ptr<Node> ueNode = ueNodes.Get (u);
    // Set the default gateway for the UE
    Ptr<Ipv4StaticRouting> ueStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
    ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
  }

  lteHelper->AttachToClosestEnb(ueLteDevs, enbLteDevs);

  // Install and start applications on UEs and remote host
  uint16_t dlPort = 1100;
  uint32_t packetSize = 500;
  string dataRate = "100Gb/s";
  ApplicationContainer clientApps, serverApps;
  for (uint32_t i = 0; i < paras.number_of_subj; ++i) {
    ++dlPort;
    OnOffHelper DlOnOff("ns3::UdpSocketFactory", Address(InetSocketAddress(ueIpIface.GetAddress (i), dlPort)));
    DlOnOff.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=0.00000004]")); // unit is second
    DlOnOff.SetAttribute ("OffTime",StringValue ("ns3::ExponentialRandomVariable[Mean=0.1|Bound=0.4]"));
    DlOnOff.SetAttribute ("PacketSize", UintegerValue (packetSize));
    DlOnOff.SetAttribute ("DataRate", DataRateValue(DataRate (dataRate)));
    DlOnOff.SetAttribute ("MaxBytes", UintegerValue (0));
    clientApps.Add (DlOnOff.Install (remoteHost));

    std::string nodeId = std::to_string(remoteHost->GetId());
    uint32_t appIdx = remoteHost->GetNApplications() - 1;
    Config::Connect("/NodeList/" + nodeId + "/ApplicationList/" + to_string(appIdx) + "/$ns3::OnOffApplication/TxWithAddresses",
      MakeCallback(&subjTxTrace));

    PacketSinkHelper DlSink("ns3::UdpSocketFactory",InetSocketAddress(ueIpIface.GetAddress (i),dlPort));
    serverApps.Add (DlSink.Install (ueNodes.Get (i)));

    nodeId = std::to_string(ueNodes.Get(i)->GetId());
    Config::Connect("/NodeList/" + nodeId + "/ApplicationList/0/$ns3::PacketSink/RxWithAddresses",
      MakeCallback(&subjRxTrace));
  }
  for (uint32_t i = 0; i < paras.number_of_intf_ue; ++i) {
    ++dlPort;
    uint32_t j = i + paras.number_of_subj;
    OnOffHelper DlOnOff("ns3::UdpSocketFactory", Address(InetSocketAddress(ueIpIface.GetAddress (j), dlPort)));
    DlOnOff.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=0.00000004]")); // unit is second
    DlOnOff.SetAttribute ("OffTime",StringValue ("ns3::ExponentialRandomVariable[Mean=0.1|Bound=0.4]"));
    DlOnOff.SetAttribute ("PacketSize", UintegerValue (packetSize));
    DlOnOff.SetAttribute ("DataRate", DataRateValue(DataRate (dataRate)));
    DlOnOff.SetAttribute ("MaxBytes", UintegerValue (0));
    clientApps.Add (DlOnOff.Install (remoteHost));

    std::string nodeId = std::to_string(remoteHost->GetId());
    uint32_t appIdx = remoteHost->GetNApplications() - 1;
    Config::Connect("/NodeList/" + nodeId + "/ApplicationList/" + to_string(appIdx) + "/$ns3::OnOffApplication/TxWithAddresses",
      MakeCallback(&intfTxTrace));

    PacketSinkHelper DlSink("ns3::UdpSocketFactory",InetSocketAddress(ueIpIface.GetAddress (j),dlPort));
    serverApps.Add (DlSink.Install (ueNodes.Get (j)));
  }

  Simulator::Stop (simTime);

  Simulator::Run ();

  Simulator::Destroy ();

  return 0;
}

Paras gen_para() {
  unsigned int number_of_subj = randomInt(1, 60);
  unsigned int number_of_intf_ue = randomInt(1, 60);
  return Paras(number_of_subj, number_of_intf_ue);
}

int main(int argc, char const *argv[]) {
  pid_t ppid = getpid();
  cout << ppid << endl;
  const unsigned int cores = std::thread::hardware_concurrency();
  const unsigned int N = 20000;
  const unsigned int workload = (N + cores - 1) / cores; // rounding up
  for (unsigned int i = 0; i < cores; ++i) {
    pid_t pid = fork();
    unsigned int offset = workload * i;
    if (pid == 0) {
      idx = (N / cores) * i;
      for (unsigned int j = 1; j <= workload; ++j) {
        if (offset + j > N) break;
        runDL(gen_para());
      }
      return 0;
    }
    else {
      cout << "Core " << i << " " << offset << ", " << offset + workload << endl;
    }
  }
  while(wait(NULL) > 0); // parent process wait for all of child process to terminate.
  // When there is no child process, error code -1 raise and end while loop

  // Paras para = gen_para();
  // runDL(para);
  return 0;
}
