#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/lte-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/geographic-positions.h"
#include "ns3/stats-module.h"

#include <iostream>
#include <thread>
#include <random>
#include <unistd.h>
#include <sys/wait.h>

using namespace std;
using namespace ns3;

random_device rdDev;
uint32_t rdSeed = 100;
mt19937 gen(rdDev());

std::unordered_map<uint32_t, uint64_t> rntiToImsi;
std::ofstream ofSinr;
std::ofstream ofUlSinr;
std::ofstream ofEnbIntf;
std::ofstream ofUlUeTxPower;

int randomInt(int left, int right) {
  static bool isSown = false;
  if (!isSown) {
    gen.seed(rdSeed);
    isSown = true;
  }
  uniform_int_distribution<int> dist(left, right);
  return dist(gen);
} 

double randomDouble (double left, double right) {
  static bool isSown = false;
  if (!isSown) {
    gen.seed(rdSeed);
    isSown = true;
  }
  uniform_real_distribution<double> dist(left, right);
  return dist(gen);
}

uint32_t combineCellIdRNTI(uint16_t cellId, uint16_t rnti) {
    uint32_t res = (uint32_t) cellId;
    res = (res << 16) + rnti;
    return res;
}

void ReportSrsTxPowerTrace(uint16_t cellId, uint16_t rnti, double SrsTxPower){
  try {
    uint64_t imsi = rntiToImsi.at(combineCellIdRNTI(cellId, rnti));
    ofUlUeTxPower << Simulator::Now() << "," << cellId << "," << rnti << "," << imsi << "," << SrsTxPower << endl;
  } catch (std::out_of_range& e) {
    cout << e.what() << endl;
  }
}

void RsrpSinrTracedCallback (uint16_t cellId, uint16_t rnti, double rsrp, double sinr, uint8_t cqi, uint8_t componentCarrierId)  {
  try {
    uint64_t imsi = rntiToImsi.at(combineCellIdRNTI(cellId, rnti));
    ofSinr.precision(17);
    ofSinr << Simulator::Now() << "," << imsi << "," << cellId << "," << rnti << "," << rsrp << "," << sinr << "," << (uint32_t) cqi << endl;
  } catch (std::out_of_range& e) {}
}

void NotifyHandoverStartUe (std::string context,
                       uint64_t imsi,
                       uint16_t cellId,
                       uint16_t rnti,
                       uint16_t targetCellId) {
  std::cout << Simulator::Now ().GetSeconds () << " " << context
            << " UE IMSI " << imsi
            << ": previously connected to CellId " << cellId
            << " with RNTI " << rnti
            << ", doing handover to CellId " << targetCellId
            << std::endl;
}

void NotifyHandoverEndOkUe (std::string context,
                       uint64_t imsi,
                       uint16_t cellId,
                       uint16_t rnti) {
  std::cout << Simulator::Now ().GetSeconds () << " " << context
            << " UE IMSI " << imsi
            << ": successful handover to CellId " << cellId
            << " with RNTI " << rnti
            << std::endl;
}

void NotifyHandoverStartEnb (std::string context,
                        uint64_t imsi,
                        uint16_t cellId,
                        uint16_t rnti,
                        uint16_t targetCellId) {
  std::cout << Simulator::Now ().GetSeconds () << " " << context
            << " eNB CellId " << cellId
            << ": start handover of UE with IMSI " << imsi
            << " RNTI " << rnti
            << " to CellId " << targetCellId
            << std::endl;
}

void NotifyHandoverEndOkEnb (std::string context,
                        uint64_t imsi,
                        uint16_t cellId,
                        uint16_t rnti) {
  std::cout << Simulator::Now ().GetSeconds () << " " << context
            << " eNB CellId " << cellId
            << ": completed handover of UE with IMSI " << imsi
            << " RNTI " << rnti
            << std::endl;
}

void NotifyConnectionEstablishedUe (std::string context,
                               uint64_t imsi,
                               uint16_t cellid,
                               uint16_t rnti) {
  // cout << "Connection established "
  //     << " UE IMSI " << imsi
  //     << ": connected to CellId " << cellid
  //     << " with RNTI " << rnti
  //     << "Updateing ue entry" << endl;
  rntiToImsi.emplace(combineCellIdRNTI(cellid, rnti), imsi);
}

void UlSinrTraceSink (uint16_t cellId, uint16_t rnti, double sinrLinear, uint8_t componentCarrierId) {
  try {
    uint64_t imsi = rntiToImsi.at(combineCellIdRNTI(cellId, rnti));
    ofUlSinr << Simulator::Now() << "," << imsi << "," << cellId << "," << rnti << "," << sinrLinear << endl;
  } catch (std::out_of_range& e) {}
}

void eNBInterferenceTraceSink(uint16_t cellId, Ptr< SpectrumValue > spectrumValue) {
  ofEnbIntf << Simulator::Now() << "," << cellId << "," << *spectrumValue << endl;
}

int runDL(double txPower, double distance, uint8_t bandwidth, uint8_t subbandwidth, uint8_t subbandoffset) {
  cout << "Tx Power: " << txPower << " Distance: " << distance << " Bandwidth: " << (int) bandwidth
    << " Subband width: " << (int) subbandwidth << " Subband offset: " << (int) subbandoffset << endl;
  std::string prefix = "/home/rui/work/ns-3-dev/model-data/" +
    to_string(txPower) + "txPower-" +
    to_string(distance) + "m" +
    to_string((int) bandwidth) + "BW" +
    to_string((int) subbandwidth) + "sub_BW" +
    to_string((int) subbandoffset) + "sub_off";

  rntiToImsi = {};
  ofSinr = std::ofstream (prefix + "-DL-SINR.csv");
  ofSinr << "Time stamp,IMSI,Cell ID,RNTI,RSRP,SINR,CQI" << endl;

  Time simTime = MilliSeconds (50);

  Config::SetDefault("ns3::LteEnbRrc::AdmitHandoverRequest", BooleanValue(false));
  Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity" ,UintegerValue(320));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));
  // Switch for DL SINR method
  // Config::SetDefault ("ns3::LteHelper::UsePdschForCqiGeneration", BooleanValue(false));

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();

  // uint8_t bandwidth = 100; // measured in number of RBs
  // uint8_t num_RB = 16;
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetFfrAlgorithmType ("ns3::LteFrHardAlgorithm");
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandOffset", UintegerValue (subbandoffset));
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandwidth", UintegerValue (subbandwidth));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandOffset", UintegerValue (subbandoffset));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandwidth", UintegerValue (subbandwidth));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));

  // Change the DN link frequency to 2110MHz, UL to 1920MH
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (0));
  lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (18000));

  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  // Install internet stack on remote host
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create internet between pgw and remote host
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (10)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  // in this container, interface 0 is the pgw, 1 is the remoteHost
  // Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  NodeContainer enbNodes;
  enbNodes.Create (2);
  NodeContainer ueNodes;
  ueNodes.Create(2);

  MobilityHelper mobility;
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (enbNodes);
  mobility.Install (ueNodes);

  enbNodes.Get(0)->GetObject<MobilityModel> ()->SetPosition (
    Vector (0.0, -300.0, 0.0)
  );
  enbNodes.Get(1)->GetObject<MobilityModel> ()->SetPosition (
    Vector (0.0, distance, 0.0)
  );

  ueNodes.Get(0)->GetObject<MobilityModel> ()->SetPosition (
    Vector (0.0, 0.0, 0.0)
  );

  ueNodes.Get(1)->GetObject<MobilityModel> ()->SetPosition (
    Vector (0.0, distance + 50.0, 0.0)
  );

  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  enbNodes.Get (0)->GetDevice (0)->GetObject<LteEnbNetDevice> ()->GetRrc ()->SetCsgId (0, true);
  ueNodes.Get (0)->GetDevice (0)->GetObject<LteUeNetDevice> ()->SetCsgId(0);
  enbNodes.Get (1)->GetDevice (0)->GetObject<LteEnbNetDevice> ()->GetRrc ()->SetCsgId (1, true);
  ueNodes.Get (1)->GetDevice (0)->GetObject<LteUeNetDevice> ()->SetCsgId(1);

  enbNodes.Get (1)->GetDevice (0)->GetObject<LteEnbNetDevice> ()->GetPhy()->SetTxPower (txPower);

  // Install IP stack on UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Assign IP address to UEs, and install applications
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u) {
    Ptr<Node> ueNode = ueNodes.Get (u);
    // Set the default gateway for the UE
    Ptr<Ipv4StaticRouting> ueStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
    ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
  }

  lteHelper->Attach (ueLteDevs.Get(0), enbLteDevs.Get (0));
  lteHelper->Attach (ueLteDevs.Get(1), enbLteDevs.Get (1));

  // Install and start applications on UEs and remote host
  uint16_t dlPort = 1100;
  Time interval = MilliSeconds(10.0);
  Time startTimes = MilliSeconds(0.0);
  ApplicationContainer clientApps, serverApps;
  for (uint32_t i = 0; i < ueNodes.GetN (); ++i) {
    ++dlPort;
    UdpClientHelper dlClientHelper (ueIpIface.GetAddress (i), dlPort);
    dlClientHelper.SetAttribute("Interval", TimeValue(interval));
    dlClientHelper.SetAttribute("StartTime", TimeValue(startTimes));
    clientApps.Add (dlClientHelper.Install (remoteHost));
    PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory",
                                         InetSocketAddress (Ipv4Address::GetAny (), dlPort));
    serverApps.Add (dlPacketSinkHelper.Install (ueNodes.Get(i)));
  }

  for (uint32_t i = 0; i < ueNodes.GetN(); ++i) {
    std::string nodeId = std::to_string(ueNodes.Get(i)->GetId());
    Config::ConnectWithoutContext ("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
      MakeCallback(&RsrpSinrTracedCallback));
    Config::Connect ("/NodeList/" + nodeId + "/DeviceList/*/LteUeRrc/ConnectionEstablished",
      MakeCallback (&NotifyConnectionEstablishedUe));
  }

  Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverStart",
                 MakeCallback (&NotifyHandoverStartEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverStart",
                 MakeCallback (&NotifyHandoverStartUe));
  Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverEndOk",
                 MakeCallback (&NotifyHandoverEndOkEnb));
  Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverEndOk",
                 MakeCallback (&NotifyHandoverEndOkUe));

  Simulator::Stop (simTime);

  Simulator::Run ();

  Simulator::Destroy ();

  return 0;
}

class Paras {
public:
  Paras(double d, double txP, uint8_t bandwidth,
    uint8_t subbandwidth, uint8_t subbandoffset):
    distance(d),
    txPower(txP),
    bw(bandwidth),
    subbandwidth(subbandwidth),
    subbandoffset(subbandoffset)
  {}
  double distance;
  double txPower;
  uint8_t bw;
  uint8_t subbandwidth;
  uint8_t subbandoffset;
};

const vector<uint8_t> bws {15, 25, 50, 75, 100};

void helper(const vector<Paras>& parameters) {
  for (auto const para : parameters) {
    runDL(para.txPower, para.distance, para.bw,
      para.subbandwidth, para.subbandoffset);
  }
}

Paras gen_para() {
  double distance = randomDouble(100.0, 10000.0);
  double txPower = randomDouble(15.0, 60.0);
  uint8_t bwIdx = randomInt(0, 4);
  uint8_t subbandwidth = randomInt(6, bws[bwIdx]);
  uint8_t subbandoffset = randomInt(0, bws[bwIdx]);
  while ((subbandwidth + subbandoffset) > bws[bwIdx]) {
    distance = randomDouble(100.0, 10000.0);
    txPower = randomDouble(15.0, 60.0);
    bwIdx = randomInt(0, 4);
    subbandwidth = randomInt(6, bws[bwIdx]);
    subbandoffset = randomInt(0, bws[bwIdx]);
  }
  return Paras(distance, txPower, bws[bwIdx], subbandwidth, subbandoffset);
}

int main(int argc, char const *argv[]) {
  pid_t ppid = getpid();
  cout << ppid << endl;
  const unsigned int cores = std::thread::hardware_concurrency();
  vector<vector<Paras>> parameters(cores);
  const unsigned int N = 10000;
  for (unsigned int i = 0; i < cores; ++i) {
    for (unsigned int j = 0; j < (N / cores); ++j) {
      parameters[i].push_back(gen_para());
    }
  }
  for (unsigned int i = 0; i < cores; ++i) {
    pid_t pid = fork();
    if (pid == 0) {
      helper(parameters[i]);
      return 0;
    }
  }
  wait(NULL);

  return 0;
}
