/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/core-module.h"
#include "ns3/spy.h"

using namespace ns3;

int 
main (int argc, char *argv[])
{
  bool verbose = true;

  CommandLine cmd (__FILE__);
  cmd.AddValue ("verbose", "Tell application to log if true", verbose);

  cmd.Parse (argc,argv);

  RealSocket rsocket{};
  rsocket.startSocket();
  rsocket.startConnThread();
  rsocket.popInQ();

//  Time t {"10s"};
//  Simulator::Schedule(t, &RealSocket::stopEvent, &rsocket);
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}


