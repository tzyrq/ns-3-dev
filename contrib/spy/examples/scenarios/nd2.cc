/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011-2018 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Rui Zuo
 */

#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/lte-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/geographic-positions.h"
#include "ns3/spy-module.h"
#include "csv.h"
#include <iostream>

using namespace ns3;

class OnOffApplicationInstaller : public IApplicationInstaller {
  private:
    uint16_t dlPort = 1100;
    Time interval = MilliSeconds(1.0);
    Time startTimes = MilliSeconds(0.0);
    uint32_t packetSize = 500;
    string dataRate = "100Gb/s";
  public:
    ApplicationContainer clientApps, serverApps;
  public:
    virtual void install(ns3::Ptr<ns3::Node> client, ns3::Ptr<ns3::Node> server, ns3::Ipv4Address address) override;
};

void OnOffApplicationInstaller::install(Ptr<Node> client, Ptr<Node> server, Ipv4Address address) {
    OnOffHelper DlOnOff("ns3::UdpSocketFactory", Address(InetSocketAddress(address, dlPort)));
    DlOnOff.SetAttribute("StartTime", TimeValue(startTimes));
    DlOnOff.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=0.00000004]")); // unit is second
    DlOnOff.SetAttribute ("OffTime",StringValue ("ns3::ExponentialRandomVariable[Mean=0.1|Bound=0.4]"));
    DlOnOff.SetAttribute ("PacketSize", UintegerValue (packetSize));
    DlOnOff.SetAttribute ("DataRate", DataRateValue(DataRate (dataRate)));
    //* 0 means no limits for packets number
    DlOnOff.SetAttribute ("MaxBytes", UintegerValue (0));
    clientApps.Add (DlOnOff.Install (client));
    PacketSinkHelper DlSink("ns3::UdpSocketFactory",InetSocketAddress(address,dlPort));
    serverApps.Add (DlSink.Install (server));
}

std::unordered_map<uint32_t, uint64_t> rntiToImsi;
std::ofstream ofSinr;

uint32_t combineCellIdRNTI(uint16_t cellId, uint16_t rnti) {
  uint32_t res = (uint32_t) cellId;
  res = (res << 16) + rnti;
  return res;
}

void RsrpSinrTracedCallback (uint16_t cellId, uint16_t rnti, double rsrp, double sinr, uint8_t cqi, uint8_t componentCarrierId)  {
  try {
    uint64_t imsi = rntiToImsi.at(combineCellIdRNTI(cellId, rnti));
    ofSinr.precision(17);
    ofSinr << Simulator::Now() << "," << imsi << "," << cellId << "," << rnti << "," << rsrp << "," << sinr << "," << (uint32_t) cqi << endl;
  } catch (std::out_of_range& e) {
    // throw std::runtime_error(e.what());
  }
}

void NotifyConnectionEstablishedUe (std::string context,
                               uint64_t imsi,
                               uint16_t cellid,
                               uint16_t rnti) {
  std::cout << "Rui: NotifyConnectionEstablishedUe" << std::endl;
  rntiToImsi.emplace(combineCellIdRNTI(cellid, rnti), imsi);
}

void NotifyHandoverEndOkUe (std::string context,
                       uint64_t imsi,
                       uint16_t cellId,
                       uint16_t rnti) {
  std::cout << "Rui: NotifyHandoverEndOkUe" << std::endl;
  rntiToImsi.emplace(combineCellIdRNTI(cellId, rnti), imsi);
}

NS_LOG_COMPONENT_DEFINE ("ND0");

int main (int argc, char *argv[]) {
  std::cout << "Reading and Parsing base station configuration from nd1.csv" << std::endl;
  io::CSVReader<3, io::trim_chars<' ', '\t'>, io::double_quote_escape<',', '"'>> in (
      "/home/rzuo02/work/ns-3-dev/contrib/spy/examples/scenarios/nd2.csv");
  in.read_header (io::ignore_extra_column, "Cell Tower Assignment (#)", "Coordinates", "TxPower");//, "Bandwidth", "SubBandwidth", "SubBandOffset");
  std::unordered_map<int, std::vector<std::string>> bsCoor;
  {
    std::string assign_number;
    std::string coor;
    std::string txPower;

    while (in.read_row (assign_number, coor, txPower))
      {
        if (bsCoor.find (std::stoi (assign_number)) != bsCoor.end ())
          continue;
        size_t pos = coor.find (",");
        string lat = coor.substr (0, pos), lng = coor.substr (pos + 1);
        std::cout << "# " << assign_number << " Lat = " << lat << " Lng = " << lng << std::endl;
        bsCoor.insert ({std::stoi (assign_number), {lat, lng, txPower}});
      }
  }
  int nBS = bsCoor.size ();
  int nUE = 300;
  Time simTime = Minutes (1000);
  int port = 62700;

  ofSinr = std::ofstream ("intf.csv");
  ofSinr << "Time stamp,IMSI,Cell ID,RNTI,RSRP,SINR,CQI" << endl;

  CommandLine cmd (__FILE__);
  cmd.AddValue ("simTime", "Total duration of the simulation", simTime);
  cmd.AddValue ("socketPort", "socket port", port);
  cmd.Parse (argc, argv);

  Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity" ,UintegerValue(320));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();

  // Set Handover Algorithm
  // Automatic handover
  lteHelper->SetHandoverAlgorithmType ("ns3::A2A4RsrqHandoverAlgorithm");
  // lteHelper->SetHandoverAlgorithmAttribute ("ServingCellThreshold", UintegerValue (30));
  // lteHelper->SetHandoverAlgorithmAttribute ("NeighbourCellOffset", UintegerValue (1));

  const uint8_t bandwidth = 15;
  const uint8_t subbandwidth = 6;
  const uint8_t subbandoffset = 1;
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetFfrAlgorithmType ("ns3::LteFrHardAlgorithm");
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandOffset", UintegerValue (subbandoffset));
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandwidth", UintegerValue (subbandwidth));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandOffset", UintegerValue (subbandoffset));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandwidth", UintegerValue (subbandwidth));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));

  // Change the DN link frequency to 2110MHz, UL to 1920MH
  lteHelper->SetEnbDeviceAttribute ("DlEarfcn", UintegerValue (0));
  lteHelper->SetEnbDeviceAttribute ("UlEarfcn", UintegerValue (18000));

  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  // Install internet stack on remote host
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create internet between pgw and remote host
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (10)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  // interface 0 is localhost, 1 is the p2p device
  // Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  NodeContainer ueNodes;
  NodeContainer enbNodes;
  enbNodes.Create (nBS);
  ueNodes.Create (nUE);

  MobilityHelper bsMobility;
  bsMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  bsMobility.Install (enbNodes);

  std::cout << "Setting Location for base station" << std::endl;
  std::unordered_map<int, Ptr<Node>> baseStationId2pNode;
  ns3::NodeContainer::Iterator enbIt = enbNodes.Begin();
  for (const auto &e : bsCoor) {
    std::vector<std::string> data = e.second;
    std::string lat = data[0], lng = data[1];
    std::string txPower = data[2];

    //* Setting location for basestation
    Vector bsPosition = GeographicPositions::GeographicToCartesianCoordinates (
        std::stod (lat), std::stod (lng), 0.0, GeographicPositions::SPHERE);
    Ptr<MobilityModel> enbMob = (*enbIt)->GetObject<MobilityModel> ();
    enbMob->SetPosition (bsPosition);
    baseStationId2pNode.insert({e.first, *enbIt});
    enbIt++;
  }

  // Install mobility for ue and this will put ue at (0,0,0) as no waypoint added currently
  MobilityHelper ueMobility;
  ueMobility.SetMobilityModel ("ns3::WaypointMobilityModel");
  ueMobility.Install (ueNodes);

  // Install LTE devices on UEs and eNB
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  for (uint32_t u = 0; u < enbNodes.GetN(); ++u) {
    enbNodes.Get(u)->GetDevice (0)->GetObject<LteEnbNetDevice> ()->GetPhy()
        ->SetTxPower (40.0);
  }

  // Install IP stack on UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Assign IP address to UEs, and install applications
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
      Ptr<Node> ueNode = ueNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv4StaticRouting> ueStaticRouting =
          ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    }

  Manager manager(ueNodes, baseStationId2pNode);

  // Install and start applications on UEs and remote host
  uint16_t dlPort = 1100;
  // Time startTimes = simTime;
  Time startTimes = Seconds(0.0);
  uint32_t packetSize = 500;
  string dataRate = "100Gb/s";
  ApplicationContainer clientApps, serverApps;
  for (uint32_t i = 0; i < ueNodes.GetN (); ++i) {
    ++dlPort;
    OnOffHelper DlOnOff("ns3::UdpSocketFactory", Address(InetSocketAddress(ueIpIface.GetAddress (i), dlPort)));
    DlOnOff.SetAttribute("StartTime", TimeValue(startTimes));
    DlOnOff.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=0.00000004]")); // unit is second
    DlOnOff.SetAttribute ("OffTime",StringValue ("ns3::ExponentialRandomVariable[Mean=0.1|Bound=0.4]"));
    DlOnOff.SetAttribute ("PacketSize", UintegerValue (packetSize));
    DlOnOff.SetAttribute ("DataRate", DataRateValue(DataRate (dataRate)));
    //* 0 means no limits for packets number
    DlOnOff.SetAttribute ("MaxBytes", UintegerValue (0));
    ApplicationContainer apps = DlOnOff.Install(remoteHost);
    clientApps.Add (apps);
    uint32_t nodeId = ueNodes.Get(i)->GetId();
    manager.ueNodeId2DlApps[nodeId] = apps;
    PacketSinkHelper DlSink("ns3::UdpSocketFactory",InetSocketAddress(ueIpIface.GetAddress (i),dlPort));
    serverApps.Add (DlSink.Install (ueNodes.Get (i)));
  }

  // Install X2 handover interface between each pair of eNb
  lteHelper->AddX2Interface (enbNodes);

  // Start socket: accept(), read()
  RealSocket rsocket{port};
  rsocket.startSocket ();
  rsocket.startConnThread ();

  OnOffApplicationInstaller appInstaller;
  // Set spy application
  Ptr<Spy> spyApp = CreateObject<Spy> (true);
  spyApp->setRealSocket (&rsocket);
  spyApp->setUeNodes (&ueNodes);
  spyApp->setEnbNodes (&enbNodes);
  spyApp->setLteHelper (lteHelper);
  spyApp->setUeLteDevs (ueLteDevs);
  spyApp->setEnbLteDevs (enbLteDevs);
  spyApp->setApplicationInstaller(appInstaller);
  // Manager manager(ueNodes, enbNodes);
  // Manager manager(ueNodes, baseStationId2pNode);
  spyApp->setManager (&manager);
  spyApp->SetStartTime (MilliSeconds (100));
  remoteHost->AddApplication (spyApp);

  // UE trace sinks
  for (NodeContainer::Iterator it = ueNodes.Begin(); it != ueNodes.End(); it++) {
    std::string nodeId = std::to_string((*it)->GetId());
    //* Update UeEntry.cellId, Manager.ueRNTIToUeEntry
    Config::Connect ("/NodeList/" + nodeId + "/DeviceList/*/LteUeRrc/HandoverEndOk",
      MakeCallback (&Spy::NotifyHandoverEndOkUe, spyApp));
    //* Update UeEntry.cellId, Manager.ueRNTIToUeEntry
    Config::Connect ("/NodeList/" + nodeId + "/DeviceList/*/LteUeRrc/ConnectionEstablished",
      MakeCallback (&Spy::NotifyConnectionEstablishedUe, spyApp));
  }

  for (NodeContainer::Iterator it = enbNodes.Begin(); it != enbNodes.End(); ++it) {
    std::string nodeId = std::to_string((*it)->GetId());
    //* Update EnbEntry.number_of_ue++
    Config::ConnectWithoutContext("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteEnbNetDevice/LteEnbRrc/ConnectionEstablished",
      MakeCallback(&Spy::NotifyConnectionEstablishedEnb, spyApp));
    //* Update EnbEntry.number_of_ue
    Config::ConnectWithoutContext("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteEnbNetDevice/LteEnbRrc/HandoverStart",
      MakeCallback(&Spy::NotifyHandoverStartEnb, spyApp));
    //* Update EnbEntry.number_of_ue
    Config::ConnectWithoutContext("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteEnbNetDevice/LteEnbRrc/HandoverEndOk",
      MakeCallback(&Spy::NotifyHandoverEndOkEnb, spyApp));
  }

  // eNB trace sinks
  // Config::Connect ("/NodeList/*/DeviceList/*/$ns3::LteNetDevice/$ns3::LteEnbNetDevice/ComponentCarrierMap/*/LteEnbPhy/RbPowerAllocation",
  //   MakeCallback (&Spy::RbPowerAllocationTraceSink, spyApp));

  Simulator::Stop (simTime);
  Simulator::Run ();

  Simulator::Destroy ();

  return 0;
}
