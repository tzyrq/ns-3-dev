/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011-2018 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Rui Zuo
 */

#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/lte-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/geographic-positions.h"
#include "ns3/spy-module.h"
#include "csv.h"
#include <iostream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("ND0");

void
TxTrace(std::string context, Ptr<const Packet> pkt, const Address& tx, const Address& rx){
    std::cout<<context<<std::endl;
    std::cout<<"\tTxTrace Size="<<pkt->GetSize()<<" at time="
             <<Simulator::Now().As (Time::S)
             <<" From Address:"<<InetSocketAddress::ConvertFrom(tx).GetIpv4()
             <<" Local Address:"<<InetSocketAddress::ConvertFrom(rx).GetIpv4()
             <<std::endl;
}

std::ostream& operator<<(std::ostream& os, const std::unordered_map<uint64_t, int>& map) {
    for (auto it = map.begin(); it != map.end(); it++) {
        os << it->first << " " << it->second << "\n";
    }
    os << std::endl;
    return os;
}

void
RxTrace(std::string context, Ptr<const Packet> pkt, const Address& tx, const Address& rx){
    std::cout<<context<<std::endl;
    std::cout<<"\tRxTrace Size="<<pkt->GetSize()<<" at time="
             <<Simulator::Now().As (Time::S)
             <<" From Address:"<<InetSocketAddress::ConvertFrom(tx).GetIpv4()
             <<" to Address:"<<InetSocketAddress::ConvertFrom(rx).GetIpv4()
             <<std::endl;
}

// Handover trace call back
void
NotifyConnectionEstablishedUe (std::string context,
                               uint64_t imsi,
                               uint16_t cellid,
                               uint16_t rnti)
{
  std::cout << Simulator::Now ().As (Time::S) << " " << context
            << " UE IMSI " << imsi
            << ": connected to CellId " << cellid
            << " with RNTI " << rnti
            << std::endl;
}

void
NotifyConnectionEstablishedEnb (std::string context,
                                uint64_t imsi,
                                uint16_t cellid,
                                uint16_t rnti)
{
  std::cout << Simulator::Now ().As (Time::S) << " " << context
            << " eNB CellId " << cellid
            << ": successful connection of UE with IMSI " << imsi
            << " RNTI " << rnti
            << std::endl;
}

void
NotifyHandoverStartUe (std::string context, uint64_t imsi, uint16_t cellId, uint16_t rnti,
                       uint16_t targetCellId)
{
  std::cout << Simulator::Now ().GetSeconds () << " " << context << " UE IMSI " << imsi
            << ": previously connected to CellId " << cellId << " with RNTI " << rnti
            << ", doing handover to CellId " << targetCellId << std::endl;
}

void
NotifyHandoverEndOkUe (std::string context, uint64_t imsi, uint16_t cellId, uint16_t rnti)
{
  std::cout << Simulator::Now ().GetSeconds () << " " << context << " UE IMSI " << imsi
            << ": successful handover to CellId " << cellId << " with RNTI " << rnti << std::endl;
}

void
NotifyHandoverStartEnb (std::string context, uint64_t imsi, uint16_t cellId, uint16_t rnti,
                        uint16_t targetCellId)
{
  std::cout << Simulator::Now ().GetSeconds () << " " << context << " eNB CellId " << cellId
            << ": start handover of UE with IMSI " << imsi << " RNTI " << rnti << " to CellId "
            << targetCellId << std::endl;
}

void
NotifyHandoverEndOkEnb (std::string context, uint64_t imsi, uint16_t cellId, uint16_t rnti)
{
  std::cout << Simulator::Now ().GetSeconds () << " " << context << " eNB CellId " << cellId
            << ": completed handover of UE with IMSI " << imsi << " RNTI " << rnti << std::endl;
}

void
PacketSinkRxCallBack (Ptr<const Packet> packet, const Address &address)
{
  std::cout << "Rx \n";
  NS_LOG_INFO ("From address = " << address);
}

void
RsrpSinrTracedCallback (uint16_t cellId, uint16_t rnti, double rsrp, double sinr,
                        uint8_t componentCarrierId)
{
  NS_LOG_INFO ("CellId = " << cellId << " rnti = " << rnti << " rsrp = " << rsrp
                           << " sinr = " << sinr);
}

void
UlPhyResourceBlocksTracedCallback (uint16_t rnti, const std::vector<int> &rbs)
{
  NS_LOG_INFO ("Total resource blocks = " << rbs.size ());
  for (auto e : rbs)
    {
      std::cout << e << " ";
    }
  std::cout << '\n';
}

void
RbPowerAllocationTraceSink (std::string context,
                            uint16_t CellId,
                            std::map<int, double> dlPowerAllocation)
{ 
  int RbUsed = 0;
  for (auto it = dlPowerAllocation.begin(); it != dlPowerAllocation.end(); ++it){
    if (it->second > 0.1)
      RbUsed++;
  }
  std::cout << "\n" << Simulator::Now().As (Time::S) << " " << context
            << "[[This is the RbPowerAllocationTraceSource]] " 
            << "CellId : " << CellId 
            << "The # of RBs used is as follows: "
            << RbUsed
            << std::endl;
//   for (auto it = dlPowerAllocation.begin(); it != dlPowerAllocation.end(); ++it)
//   {
//     std::cout << "\n" 
//               << " RbID : " << it->first
//               << " Power (dBm) : " << it->second;
//   }
//   std::cout << std::endl;
}

int
main (int argc, char *argv[])
{
  io::CSVReader<2, io::trim_chars<' ', '\t'>, io::double_quote_escape<',', '"'>> in (
      "/home/rzuo02/work/ns-3-dev/contrib/spy/examples/scenarios/nd0.csv");
  in.read_header (io::ignore_extra_column, "Cell Tower Assignment (#)", "Coordinates");
  std::unordered_map<int, std::pair<std::string, std::string>> bsCoor;
  {
    std::string assign_number;
    std::string coor;

    while (in.read_row (assign_number, coor))
      {
        if (bsCoor.find (std::stoi (assign_number)) != bsCoor.end ())
          continue;
        size_t pos = coor.find (", ");
        string lat = coor.substr (0, pos), lng = coor.substr (pos + 2);
        std::cout << "# " << assign_number << " Lat = " << lat << " Lng = " << lng << std::endl;
        bsCoor.insert ({std::stoi (assign_number), std::make_pair (lat, lng)});
      }
  }
  int nBS = bsCoor.size ();
  int nUE = 1;
  Time windowSize =  MilliSeconds (1000);
  Time simTime = Minutes (100);
  bool disableDl = false, disableUl = false;
  Time interPacketInterval = MilliSeconds (100);
  int socket_port = 62700;

  CommandLine cmd (__FILE__);
  cmd.AddValue ("nUE", "Total number of ue in lauch pool", nUE);
  cmd.AddValue ("simTime", "Total duration of the simulation", simTime);
  cmd.AddValue ("disableDl", "Disable downlink data flows", disableDl);
  cmd.AddValue ("disableUl", "Disable uplink data flows", disableUl);
  cmd.AddValue ("socketPort", "socket port", socket_port);
  cmd.Parse (argc, argv);

  Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity" ,UintegerValue(320));
  Config::SetDefault ("ns3::LteSpectrumPhy::CtrlErrorModelEnabled", BooleanValue (false));
  Config::SetDefault ("ns3::LteSpectrumPhy::DataErrorModelEnabled", BooleanValue (false));

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();

  // Set Handover Algorithm
  // Automatic handover
  lteHelper->SetHandoverAlgorithmType ("ns3::A2A4RsrqHandoverAlgorithm");
  lteHelper->SetHandoverAlgorithmAttribute ("ServingCellThreshold", UintegerValue (30));
  lteHelper->SetHandoverAlgorithmAttribute ("NeighbourCellOffset", UintegerValue (1));

  uint8_t bandwidth = 100; // measured in number of RBs
  uint8_t num_RB = 16;
  lteHelper->SetEnbDeviceAttribute ("DlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetEnbDeviceAttribute ("UlBandwidth", UintegerValue (bandwidth));
  lteHelper->SetFfrAlgorithmType ("ns3::LteFrHardAlgorithm");
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("DlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandOffset", UintegerValue (num_RB));
  lteHelper->SetFfrAlgorithmAttribute ("UlSubBandwidth", UintegerValue (num_RB));
  lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));


  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper (epcHelper);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  // Install internet stack on remote host
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create internet between pgw and remote host
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (10)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  // interface 0 is localhost, 1 is the p2p device
  Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting =
      ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  NodeContainer ueNodes;
  NodeContainer enbNodes;
  ueNodes.Create (nUE);
  enbNodes.Create (nBS);

  // Install Mobility for BS
  //Ptr<ListPositionAllocator> bsPositionAlloc = CreateObject<ListPositionAllocator> ();
  //for (auto &e : bsCoor)
  //  {
  //    std::string lat = e.second.first, lng = e.second.second;
  //    Vector bsPosition = GeographicPositions::GeographicToCartesianCoordinates (
  //        std::stod (lat), std::stod (lng), 100.0, GeographicPositions::SPHERE);
  //    bsPositionAlloc->Add (bsPosition);
  //  }

  MobilityHelper bsMobility;
  bsMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  //bsMobility.SetPositionAllocator (bsPositionAlloc);
  bsMobility.Install (enbNodes);
  // NS_LOG_INFO ("eNB at " << enbNodes.Get(0)->GetObject<MobilityModel>() ->GetPosition() << "\n");

  for (size_t i = 0; i < bsCoor.size(); i++)
  {
    std::pair<std::string, std::string> coor = bsCoor[i+1];
    std::string lat = coor.first, lng = coor.second;
    Vector bsPosition = GeographicPositions::GeographicToCartesianCoordinates (
        std::stod (lat), std::stod (lng), 100.0, GeographicPositions::SPHERE);
    Ptr<MobilityModel> enbMob = enbNodes.Get(i)->GetObject<MobilityModel> ();
    enbMob->SetPosition (bsPosition);
  }
 

  // Install mobility for ue and this will put ue at (0,0,0) as no waypoint added currently
  MobilityHelper ueMobility;
  ueMobility.SetMobilityModel ("ns3::WaypointMobilityModel");
  ueMobility.Install (ueNodes);

  // Install LTE devices on UEs and eNB
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  // Install IP stack on UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Assign IP address to UEs, and install applications
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
      Ptr<Node> ueNode = ueNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv4StaticRouting> ueStaticRouting =
          ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    }

  // Install and start applications on UEs and remote host
  uint16_t dlPort = 1100;
  uint16_t ulPort = 2000;
  ApplicationContainer dlClientApps, dlServerApps;
  ApplicationContainer ulClientApps, ulServerApps;
  uint32_t packetSize = 500;
  uint32_t maxPackets = 4294967295;
  for (uint32_t i = 0; i < ueNodes.GetN (); ++i)
    {
      if (!disableDl)
        { ++dlPort;
          UdpEchoServerHelper dlServer (dlPort);
          dlServer.SetAttribute ("CIMSI", UintegerValue (10000)); // 10000 for remoteHost
          dlServer.SetAttribute ("SIMSI", UintegerValue (ueNodes.Get(i)->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi()));
          dlServerApps.Add (dlServer.Install (ueNodes.Get (i)));

          UdpEchoClientHelper dlClient (ueIpIface.GetAddress (i), dlPort);
          dlClient.SetAttribute ("Interval", TimeValue (interPacketInterval));
          dlClient.SetAttribute ("LocalAddress", AddressValue (ueIpIface.GetAddress (i)));
          dlClient.SetAttribute ("windowSize", TimeValue (windowSize));
          dlClient.SetAttribute ("CIMSI", UintegerValue (10000));
          dlClient.SetAttribute ("SIMSI", UintegerValue (ueNodes.Get(i)->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi()));
          dlClient.SetAttribute ("MaxPackets", UintegerValue (maxPackets));
          dlClient.SetAttribute ("PacketSize", UintegerValue (packetSize));
          dlClientApps.Add (dlClient.Install (remoteHost));
        }
      if (!disableUl)
        {
          ++ulPort;
          UdpEchoServerHelper ulServer (ulPort);
          ulServer.SetAttribute ("CIMSI", UintegerValue (ueNodes.Get(i)->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi()));
          ulServer.SetAttribute ("SIMSI", UintegerValue (10000));
          ulServerApps.Add (ulServer.Install (remoteHost));

          UdpEchoClientHelper ulClient (remoteHostAddr, ulPort);
          ulClient.SetAttribute ("Interval", TimeValue (interPacketInterval));
          ulClient.SetAttribute ("windowSize", TimeValue (windowSize));
          ulClient.SetAttribute ("MaxPackets", UintegerValue (maxPackets));
          ulClient.SetAttribute ("CIMSI", UintegerValue (ueNodes.Get(i)->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi()));
          ulClient.SetAttribute ("SIMSI", UintegerValue (10000));
          ulClient.SetAttribute ("PacketSize", UintegerValue (packetSize));
          ulClientApps.Add (ulClient.Install (ueNodes.Get(i)));
        }
    }

  // Add trace source to UEs
  //for (uint32_t i = 0; i < ueNodes.GetN (); i++)
  //{
  //  Ptr<Node> ue = ueNodes.Get (i);
  ////  std::ostringstream oss;
  ////  // Adding ReportCurrentCellRsrpSinr trace sink
  ////  oss << "/NodeList/" << ue->GetId()
  ////      << "/DeviceList/" << 0  // Device 0 is LTE devs
  ////      << "/$ns3::LteUeNetDevice/ComponentCarrierMapUe/"
  ////      << 0 << "/LteUePhy/ReportCurrentCellRsrpSinr"; // Only have one component carrier
  ////  Config::ConnectWithoutContext (oss.str(),
  ////    MakeCallback(&RsrpSinrTracedCallback));

  //  std::ostringstream oss;
  //  // Adding ReportUlPhyResouceBlocks trace sink
  //  oss << "/NodeList/" << ue->GetId()
  //      << "/DeviceList/" << 0  // Device 0 is LTE devs
  //      << "/$ns3::LteUeNetDevice/ComponentCarrierMapUe/"
  //      << 0 << "/LteUePhy/ReportUlPhyResourceBlocks"; // Only have one component carrier
  //  Config::ConnectWithoutContext (oss.str(),
  //    MakeCallback(&UlPhyResourceBlocksTracedCallback));
  //}

  // Install X2 handover interface between each pair of eNb
  lteHelper->AddX2Interface (enbNodes);
  
  //Config::Connect("/NodeList/*/ApplicationList/*/$ns3::UdpEchoClient/TxWithAddresses", MakeCallback(&TxTrace));
  //Config::Connect("/NodeList/*/ApplicationList/*/$ns3::UdpEchoClient/RxWithAddresses", MakeCallback(&RxTrace));

  //Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/ConnectionEstablished",
  //                 MakeCallback (&NotifyConnectionEstablishedEnb));
  //Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/ConnectionEstablished",
  //                 MakeCallback (&NotifyConnectionEstablishedUe));
  //Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverStart",
  //                 MakeCallback (&NotifyHandoverStartEnb));
  //Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverStart",
  //                 MakeCallback (&NotifyHandoverStartUe));
  //Config::Connect ("/NodeList/*/DeviceList/*/LteEnbRrc/HandoverEndOk",
  //                 MakeCallback (&NotifyHandoverEndOkEnb));
  //Config::Connect ("/NodeList/*/DeviceList/*/LteUeRrc/HandoverEndOk",
  //                 MakeCallback (&NotifyHandoverEndOkUe));

  // Start socket: accept(), read()
  RealSocket rsocket{socket_port};
  rsocket.startSocket ();
  rsocket.startConnThread ();

  // Set spy application
  Ptr<Spy> spyApp = CreateObject<Spy> (true);
  spyApp->setRealSocket (&rsocket);
  spyApp->setUeNodes (&ueNodes);
  spyApp->setEnbNodes (&enbNodes);
  spyApp->setLteHelper (lteHelper);
  spyApp->setUeLteDevs (ueLteDevs);
  spyApp->setEnbLteDevs (enbLteDevs);
  Manager manager(ueNodes, enbNodes);
  spyApp->setManager (&manager);
  spyApp->SetStartTime (MilliSeconds (100));
  remoteHost->AddApplication (spyApp);

  // UE trace sinks
  for (NodeContainer::Iterator it = ueNodes.Begin(); it != ueNodes.End(); it++) {
    std::string nodeId = std::to_string((*it)->GetId());
    Config::ConnectWithoutContext("/NodeList/" + nodeId + "/ApplicationList/*/$ns3::UdpEchoClient/TxIMSI",
      MakeCallback(&Spy::ClientTxIMSITrace, spyApp));
    Config::ConnectWithoutContext("/NodeList/" + nodeId + "/ApplicationList/*/$ns3::UdpEchoClient/RxIMSI",
      MakeCallback(&Spy::ClientRxIMSITrace, spyApp));
    Config::ConnectWithoutContext ("/NodeList/" + nodeId + "/DeviceList/*/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
      MakeCallback(&Spy::RsrpSinrTracedCallback, spyApp));
    Config::Connect ("/NodeList/" + nodeId + "/DeviceList/*/LteUeRrc/HandoverStart",
      MakeCallback (&Spy::NotifyHandoverStartUe, spyApp));
    Config::Connect ("/NodeList/" + nodeId + "/DeviceList/*/LteUeRrc/HandoverEndOk",
      MakeCallback (&Spy::NotifyHandoverEndOkUe, spyApp));
    Config::Connect ("/NodeList/" + nodeId + "/DeviceList/*/LteUeRrc/ConnectionEstablished",
      MakeCallback (&Spy::NotifyConnectionEstablishedUe, spyApp));
    Config::ConnectWithoutContext ("/NodeList/" + nodeId + "/DeviceList/*/LteUeRrc/StateTransition",
      MakeCallback (&Spy::UeStateTransition, spyApp));
  }

  // eNB trace sinks
  Config::Connect ("/NodeList/*/DeviceList/*/$ns3::LteNetDevice/$ns3::LteEnbNetDevice/ComponentCarrierMap/*/LteEnbPhy/RbPowerAllocation",
    MakeCallback (&Spy::RbPowerAllocationTraceSink, spyApp));

  // Set start time for UE
  dlServerApps.Start (MilliSeconds (200));
  ulClientApps.Start (MilliSeconds (200));
  dlServerApps.Stop (simTime);
  ulClientApps.Stop (simTime);

  // Set start time for eNB to start immediately
  dlClientApps.Start (MilliSeconds (100));
  ulServerApps.Start (MilliSeconds (100));
  dlClientApps.Stop (simTime);
  ulServerApps.Stop (simTime);
   
  std::cout << "123" << std::endl;

  //lteHelper->EnableTraces ();

  Simulator::Stop (simTime);
  Simulator::Run ();

  Simulator::Destroy ();

  NS_LOG_INFO ("returning");
  return 0;
}
