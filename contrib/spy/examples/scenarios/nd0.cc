/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2011-2018 Centre Tecnologic de Telecomunicacions de Catalunya (CTTC)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Rui Zuo
 */

#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/lte-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/mobility-module.h"
#include "ns3/applications-module.h"
#include "ns3/geographic-positions.h"
#include "ns3/spy-module.h"
#include "csv.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("ND0");

void PacketSinkRxCallBack(Ptr< const Packet > packet, const Address &address)
{
    std::cout << "Rx \n";
    NS_LOG_INFO ("From address = " << address);
}

void RsrpSinrTracedCallback (uint16_t cellId, uint16_t rnti, double rsrp, double sinr, uint8_t componentCarrierId) 
{
  NS_LOG_INFO ("CellId = " << cellId << " rnti = " << rnti << 
    " rsrp = " << rsrp << " sinr = " << sinr);
}

void UlPhyResourceBlocksTracedCallback (uint16_t rnti, const std::vector<int> &rbs) {
  NS_LOG_INFO("Total resource blocks = " << rbs.size());
  for (auto e : rbs) {
    std::cout << e << " ";
  }
  std::cout << '\n';
}

int 
main(int argc, char *argv[])
{
  std::string csv_path = "/home/rzuo02/work/ns-3-dev/contrib/spy/examples/scenarios/nd0.csv";
  io::CSVReader<2, io::trim_chars<' ', '\t'>, io::double_quote_escape<',', '"'>> in(csv_path);
  in.read_header(io::ignore_extra_column, 
                 "Cell Tower Assignment (#)",
                 "Coordinates"
                 );
  std::unordered_map<int, std::pair<std::string, std::string>> bsCoor;
  {
      std::string assign_number;
      std::string coor;

      while (in.read_row(assign_number, coor)) {
        if (bsCoor.find(std::stoi(assign_number)) != bsCoor.end()) continue;
        size_t pos = coor.find(", ");
        string lat = coor.substr(0, pos),
               lng = coor.substr(pos+2);
        std::cout << "# " << assign_number
                  << " Lat = " << lat 
                  << " Lng = " << lng
                  << std::endl;
        bsCoor.insert({std::stoi(assign_number), std::make_pair(lat, lng)});
      }
  }
  int nBS = bsCoor.size();
  int nUE = 1;
  Time simTime = Minutes (100);
  bool disableDl = false,
       disableUl = false;
  Time interPacketInterval = MilliSeconds (100); 

  CommandLine cmd (__FILE__);
  cmd.AddValue ("nUE", "Total number of ue in lauch pool", nUE);
  cmd.AddValue ("simTime", "Total duration of the simulation", simTime);
  cmd.AddValue ("disableDl", "Disable downlink data flows", disableDl);
  cmd.AddValue ("disableUl", "Disable uplink data flows", disableUl);
  cmd.Parse (argc, argv);

  Config::SetDefault("ns3::LteEnbRrc::SrsPeriodicity" ,UintegerValue(320));

  Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> ();
  lteHelper->SetEpcHelper(epcHelper);

  Ptr<Node> pgw = epcHelper->GetPgwNode ();

  // Create a single RemoteHost
  NodeContainer remoteHostContainer;
  remoteHostContainer.Create (1);
  Ptr<Node> remoteHost = remoteHostContainer.Get (0);
  // Install internet stack on remote host
  InternetStackHelper internet;
  internet.Install (remoteHostContainer);

  // Create internet between pgw and remote host
  PointToPointHelper p2ph;
  p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
  p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
  p2ph.SetChannelAttribute ("Delay", TimeValue (MilliSeconds (10)));
  NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
  Ipv4AddressHelper ipv4h;
  ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
  Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
  // interface 0 is localhost, 1 is the p2p device 
  Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

  Ipv4StaticRoutingHelper ipv4RoutingHelper;
  Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
  remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

  NodeContainer ueNodes;
  NodeContainer enbNodes;
  ueNodes.Create (nUE);
  enbNodes.Create (nBS);

  // Install Mobility for BS
  Ptr<ListPositionAllocator> bsPositionAlloc = CreateObject<ListPositionAllocator> ();
  for (auto && e : bsCoor)
  {
    std::string lat = e.second.first,
                lng = e.second.second;
    Vector bsPosition = GeographicPositions::GeographicToCartesianCoordinates (
                          std::stod(lat), 
                          std::stod(lng), 
                          100.0, GeographicPositions::SPHERE);
    bsPositionAlloc->Add (bsPosition);
  }
  
  MobilityHelper bsMobility;
  bsMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  //bsMobility.SetPositionAllocator (bsPositionAlloc);
  bsMobility.Install (enbNodes);
  // NS_LOG_INFO ("eNB at " << enbNodes.Get(0)->GetObject<MobilityModel>() ->GetPosition() << "\n");
  for (size_t i = 0; i < bsCoor.size(); i++)
  {
    std::pair<std::string, std::string> coor = bsCoor[i+1];
    std::string lat = coor.first, lng = coor.second;
    Vector bsPosition = GeographicPositions::GeographicToCartesianCoordinates (
        std::stod (lat), std::stod (lng), 100.0, GeographicPositions::SPHERE);
    Ptr<MobilityModel> enbMob = enbNodes.Get(i)->GetObject<MobilityModel> ();
    enbMob->SetPosition (bsPosition);
  }

  // Install mobility for ue and this will put ue at (0,0,0) as no waypoint added currently
  MobilityHelper ueMobility;
  ueMobility.SetMobilityModel ("ns3::WaypointMobilityModel");
  ueMobility.Install (ueNodes);
  
  // Install LTE devices on UEs and eNB 
  NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
  NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

  // Install IP stack on UEs
  internet.Install (ueNodes);
  Ipv4InterfaceContainer ueIpIface;
  ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));

  // Assign IP address to UEs, and install applications
  for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
      Ptr<Node> ueNode = ueNodes.Get (u);
      // Set the default gateway for the UE
      Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
      ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    }
  
  // Install and start applications on UEs and remote host
  uint16_t dlPort = 1100;
  uint16_t ulPort = 2000;
  ApplicationContainer dlClientApps, dlServerApps;
  ApplicationContainer ulClientApps, ulServerApps;
  for (uint32_t i = 0; i < ueNodes.GetN (); ++i)
    {
        if (!disableDl) 
          {
            NS_LOG_INFO ("Install downlink apps");
            PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory", 
                InetSocketAddress (Ipv4Address::GetAny (), dlPort));
            auto ueApp = dlPacketSinkHelper.Install (ueNodes.Get(i));
            dlServerApps.Add (ueApp);
            //std::ostringstream oss;
            //// Adding Packet Sink app Recive sink
            //oss << "/NodeList/" << ueNodes.Get (i)->GetId()
            //    << "/ApplicationList/" << 0
            //    << "/$ns3::PacketSink/Rx";
            //Config::ConnectWithoutContext (oss.str(), 
            //    MakeCallback (&PacketSinkRxCallBack));

            UdpClientHelper dlClient (ueIpIface.GetAddress (i), dlPort);
            dlClient.SetAttribute ("Interval", TimeValue (interPacketInterval));
            dlClient.SetAttribute ("MaxPackets", UintegerValue (1000000));
            dlClientApps.Add (dlClient.Install (remoteHost));
          }
        
        if (!disableUl) 
          {
            NS_LOG_INFO ("Install Uplink apps");
            ++ulPort;
            PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory",
                InetSocketAddress (Ipv4Address::GetAny (), ulPort));
            ulServerApps.Add (ulPacketSinkHelper.Install (remoteHost));

            UdpClientHelper ulClient (remoteHostAddr, ulPort);
            ulClient.SetAttribute ("Interval", TimeValue (interPacketInterval));
            ulClient.SetAttribute ("MaxPackets", UintegerValue (1000000));
            ulClientApps.Add (ulClient.Install (ueNodes.Get(i)));
          }
    }

  // Add trace source to UEs
  //for (uint32_t i = 0; i < ueNodes.GetN (); i++)
  //{
  //  Ptr<Node> ue = ueNodes.Get (i);
  ////  std::ostringstream oss;    
  ////  // Adding ReportCurrentCellRsrpSinr trace sink
  ////  oss << "/NodeList/" << ue->GetId()
  ////      << "/DeviceList/" << 0  // Device 0 is LTE devs
  ////      << "/$ns3::LteUeNetDevice/ComponentCarrierMapUe/"
  ////      << 0 << "/LteUePhy/ReportCurrentCellRsrpSinr"; // Only have one component carrier
  ////  Config::ConnectWithoutContext (oss.str(),
  ////    MakeCallback(&RsrpSinrTracedCallback));

  //  std::ostringstream oss;    
  //  // Adding ReportUlPhyResouceBlocks trace sink
  //  oss << "/NodeList/" << ue->GetId()
  //      << "/DeviceList/" << 0  // Device 0 is LTE devs
  //      << "/$ns3::LteUeNetDevice/ComponentCarrierMapUe/"
  //      << 0 << "/LteUePhy/ReportUlPhyResourceBlocks"; // Only have one component carrier
  //  Config::ConnectWithoutContext (oss.str(),
  //    MakeCallback(&UlPhyResourceBlocksTracedCallback));
  //}
  
  // Start socket: accept(), read()
  RealSocket rsocket;
  rsocket.startSocket ();
  rsocket.startConnThread ();
  
  // Set spy application
  Ptr<Spy> spyApp = CreateObject<Spy> ();
  spyApp->setRealSocket (&rsocket);
  spyApp->setUeNodes (&ueNodes);
  spyApp->setEnbNodes (&enbNodes);
  spyApp->setLteHelper (lteHelper);
  spyApp->setUeLteDevs (ueLteDevs);
  spyApp->setEnbLteDevs (enbLteDevs);
  Manager manager(ueNodes, enbNodes);
  spyApp->setManager (&manager);
  spyApp->SetStartTime (MilliSeconds (100));
  remoteHost->AddApplication (spyApp);

  // Set start time for UE  
  dlServerApps.Start (Seconds (10));  
  ulClientApps.Start (Seconds (10));

  // Set start time for eNB to start immediately 
  dlClientApps.Start (MilliSeconds (100));  
  ulServerApps.Start (MilliSeconds (100));

  //lteHelper->EnableTraces ();

  Simulator::Stop (simTime);
  Simulator::Run ();

  Simulator::Destroy ();

  NS_LOG_INFO("returning");
  return 0;
}
