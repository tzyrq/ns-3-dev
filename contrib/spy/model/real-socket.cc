/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Rui Zuo <ruitso2018@gmail.com>
 */

#include "ns3/log.h"
#include "real-socket.h"
#include "ns3/simulator.h"

using namespace ROC;

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("RealSocket");

RealSocket::RealSocket()
  : listen_fd(0),
    conn_fd(0),
    local_port(62700) {
  NS_LOG_FUNCTION(this);
}

RealSocket::RealSocket(int local_port)
  : listen_fd(0),
    conn_fd(0),
    local_port(local_port) {
  NS_LOG_FUNCTION(this);
}

RealSocket::~RealSocket() {
  if (fcntl(listen_fd, F_GETFL) != -1)
    Close(listen_fd);
  if (fcntl(conn_fd, F_GETFL) != -1)
    Close(conn_fd);
}

void RealSocket::startSocket() {
    listen_fd = Socket(AF_INET, SOCK_STREAM, 0);
    const int enable = 1;
    if (setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
      throw std::runtime_error("setsockopt(SO_REUSEADDR) failed");
    sockaddr_in servaddr;
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(local_port);
    Inet_pton(AF_INET, "127.0.0.1", &servaddr.sin_addr);
    Bind(listen_fd, (struct sockaddr*) &servaddr, sizeof(servaddr));
    NS_LOG_INFO ("Listening on 127.0.0.1:" << local_port);
    Listen(listen_fd, LISTENQ);
    conn_fd = Accept(listen_fd, NULL, NULL);
    NS_LOG_INFO ("Accepted one");
    Close(listen_fd);
    NS_LOG_INFO ("Closing listening socket");
}

void RealSocket::startConnThread() {
    std::future<void> future = endConnTh.get_future();
    std::thread th (&RealSocket::connRead, this, std::move(future));
    connTh = std::move(th);
}

void RealSocket::connRead(std::future<void> future) {
    ssize_t n;
    ssize_t max_len = 1024;
    char buf[max_len];
again:
    NS_LOG_INFO("connRead again");
    while(
        (n = read(conn_fd, buf, max_len)) > 0
    ) {
        char* tmp = new char[sizeof(buf)];
        memcpy(tmp, buf, sizeof(buf));
        auto message = GetMessage(tmp);
        auto roc_type = message->data_type();
        switch(roc_type) {
            case MessageType_CreationReq: NS_LOG_INFO("##Create" << "\n");
                                 break;
            case MessageType_ActionReq: NS_LOG_INFO("##Action" << "\n");
                                 break;
            case MessageType_DeletionReq: NS_LOG_INFO("##Delete" << "\n");
                                 break;
            case MessageType_SINRReq: NS_LOG_INFO("##SINRReq" << "\n");
                                  break;
            case MessageType_ThroughputReq: NS_LOG_INFO("##Thrpt" << "\n");
                                        break;
            case MessageType_EnbReq: NS_LOG_INFO("##Thrpt" << "\n");
                                        break;
            default: NS_LOG_INFO("##Default" << "\n");
                    break;
        }
        inQ.push_back((uint8_t*) tmp);
        NS_LOG_INFO("inQ size = " << inQ.size() << " inQ max size " << inQ.max_size());
    }
    NS_LOG_INFO("out of while loop");
    std::cout << " n= " << n << "errno " << errno << std::endl;
    if (future.wait_for(std::chrono::milliseconds(1)) == std::future_status::timeout) {
        //if (n < 0 && errno == EINTR) {
        //    NS_LOG_INFO("n < 0 && errno == EINTR");
        //    goto again;
        //}
        if (n < 0 && errno == EAGAIN) {
            NS_LOG_INFO("n < 0 && errno == EAGAIN");
            goto again;
        }
        else if (n < 0)
            err_sys("listen_roc: read erro");
    }
    NS_LOG_INFO("return from connRead");
}

uint8_t* RealSocket::popInQ() {
    NS_LOG_FUNCTION(this);
    if (inQ.size() > 0) {
        NS_LOG_INFO("pop from front of inQ");
        auto buf_pointer = inQ.front();
        inQ.pop_front();
        return buf_pointer;
    }
    else return nullptr;
}

void RealSocket::roc_deletion_CB(const Message* message) {
    auto deletion_info = static_cast<const DeletionReq*>(message->data());
    auto uav_id = deletion_info->uav_id()->str();
    NS_LOG_INFO("roc_deletion_CB(): "<< "id=" << uav_id << std::endl);
}

void RealSocket::roc_creation_CB(const Message* message) {
    auto creation_info = static_cast<const CreationReq*>(message->data());
    auto uav_id = creation_info->uav_id()->str();
    auto lat = creation_info->latitude()->str();
    auto lng = creation_info->longitude()->str();
    auto master_id = creation_info->master_id();
    NS_LOG_INFO("roc_creation_CB(): "
        << "id=" << uav_id
        << " lat=" << lat
        << " lng=" << lng
        << " master_id=" << master_id << std::endl);
}

void RealSocket::roc_action_CB(const Message* message) {
    auto actionInfoRecieved = static_cast<const ROC::ActionReq*>(message->data());
    auto uav_id = actionInfoRecieved->uav_id()->str();
    auto lat = actionInfoRecieved->latitude()->str();
    auto lng = actionInfoRecieved->longitude()->str();
    NS_LOG_INFO("roc_action_CB(): "
        << "id=" << uav_id
        << " lat=" << lat
        << " lng=" << lng << std::endl);
}

void RealSocket::stopEvent() {
    NS_LOG_FUNCTION(this);
    Simulator::Cancel(m_event);
    endConnTh.set_value();
    connTh.join();
}

void RealSocket::stopTh() {
    NS_LOG_FUNCTION(this);
    endConnTh.set_value();
    connTh.join();
}

ssize_t RealSocket::Write(uint8_t* buf, int n) {
    NS_LOG_FUNCTION(this);
    std::unique_lock<std::mutex> lock(m_write);
    int size = htonl(n);
    int res = write(conn_fd, (const void*) &size, sizeof(size));
    NS_LOG_INFO("wirte() returned : " << res << "\n");
    //std::cout << "n = " << n << "\n";
    res = write(conn_fd, (const void*) buf, n);
    NS_LOG_INFO("wirte() returned : " << res << "\n");
    return 0;
}

} /* namespace ns3 */

