/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef SPY_H
#define SPY_H

#include "ns3/lte-module.h"
#include "ns3/application.h"
#include "manager.h"
#include "real-socket.h"
#include "application-installer.h"
#include "ROC_generated.h"
#include "roc-builder.h"

namespace ns3 {

class Spy : public Application {
public:
    static TypeId GetTypeId (void);
    Spy();
    Spy(bool);
    Spy(RealSocket*);
    virtual ~Spy();
    void setRealSocket(RealSocket*);
    void setUeNodes(NodeContainer*);
    void setEnbNodes(NodeContainer*);
    void setLteHelper (Ptr<LteHelper>&);
    void setUeLteDevs (NetDeviceContainer&);
    void setEnbLteDevs (NetDeviceContainer&);
    void setManager (Manager*);
    void setApplicationInstaller(IApplicationInstaller &appInstaller);
private:
    virtual void StartApplication (void);    // Called at time specified by Start
    virtual void StopApplication (void);     // Called at time specified by Stop
    void ScheduleStartEvent(void);
    void ScheduleNextRead(void);
    void StartReadingRealSocket(void);
    void ReadRealSocket(void);
    void ScheduleStopEvent(void);

    void roc_creation_CB(const ROC::Message*, uint8_t*);
    void ScheduleCreation (Time t ,const ROC::Message*, uint8_t*);
    void roc_action_CB(const ROC::Message*, uint8_t*);
    void ScheduleAction (Time t ,const ROC::Message*, uint8_t*);
    void ScheduleActionNow (const ROC::Message*, uint8_t*);
    void roc_deletion_CB(const ROC::Message*, uint8_t*);
    void ScheduleDeletion (Time t ,const ROC::Message*, uint8_t*);
    /* SINR Start */
    void roc_SINRReq_CB(const ROC::Message*, uint8_t*);
    void ScheduleSINRReq (Time t ,const ROC::Message*, uint8_t*);
    void ScheduleSINRReqNow (const ROC::Message*, uint8_t*);
public:
    void RsrpSinrTracedCallback (uint16_t, uint16_t, double, double, uint8_t, uint8_t);
    /* SINR End */
private:
    /* Throughput Start */
    void roc_ThroughputReq_CB (const ROC::Message*, uint8_t*);
    void ScheduleThroughputReq (Time, const ROC::Message*, uint8_t*);
public:
    void ClientTxIMSITrace (Ptr<const Packet>, Time, uint32_t, uint32_t, uint32_t);
    void ClientRxIMSITrace(Ptr<const Packet>, Time, uint32_t, uint32_t, uint32_t);
    /* Throughput End */
public:
    // UE handover call back
    void NotifyHandoverStartUe (std::string, uint64_t, uint16_t, uint16_t, uint16_t);
    void NotifyHandoverEndOkUe (std::string, uint64_t, uint16_t, uint16_t);
    void NotifyConnectionEstablishedUe(std::string, uint64_t imsi, uint16_t cellid, uint16_t rnti);
    void UeStateTransition (uint64_t, uint16_t, uint16_t, LteUeRrc::State, LteUeRrc::State);
    /* eNB Req start */
    void RbPowerAllocationTraceSink (std::string, uint16_t, std::map<int, double>);
    void NotifyConnectionEstablishedEnb (uint64_t imsi,uint16_t cellid,uint16_t rnti);
    void NotifyConnectionReleaseAtEnodeB (uint64_t imsi, uint16_t cellId, uint16_t rnti);
    void NotifyConnectionReconfiguration (const uint64_t imsi,const uint16_t cellId, const uint16_t rnti);
    void NotifyHandoverStartEnb(const uint64_t imsi,const uint16_t cellId, const uint16_t rnti, uint16_t targetCellId);
    void NotifyHandoverEndOkEnb(const uint64_t imsi,const uint16_t cellId, const uint16_t rnti);
private:
    void roc_EnbReq_CB (const ROC::Message*, uint8_t*);
    void ScheduleEnbReq (Time, const ROC::Message*, uint8_t*);
    void roc_UeReq_CB (const ROC::Message*, uint8_t*);
    void ScheduleUeReq (Time, const ROC::Message*, uint8_t*);
    /* eNB Req end */
public:
    bool writeToFile;
    std::ofstream ofSinr;
private:
    EventId m_startEvent;
    EventId m_readEvent;
    Time m_lastReadingTime;
private:
    RealSocket* rsocket;
    NetDeviceContainer ueLteDevs;
    NetDeviceContainer enbLteDevs;
    NodeContainer* ueNodes;
    NodeContainer* enbNodes;
    std::unordered_map<std::string, uint32_t> idToIdx;
    Ptr<LteHelper> lteHelper;
    Manager* manager;
    ROCBuilder rocBuilder;
    IApplicationInstaller *appInstaller;
};


} /* namespace ns3 */

#endif /* SPY_H */

