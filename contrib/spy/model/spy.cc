/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/simulator.h"
#include "ns3/core-module.h"
#include "ns3/mobility-module.h"
#include "ns3/internet-module.h"
#include "ns3/packet-sink.h"
#include "ns3/geographic-positions.h"
#include "spy.h"
#include <iostream>

using namespace ROC;

namespace ns3 {

std::ostream& operator<<(std::ostream& os, std::unordered_map<uint64_t, int>& map) {
    for (auto it = map.begin(); it != map.end(); it++) {
        os << it->first << " " << it->second << "\n";
    }
    os << std::endl;
    return os;
}
std::ostream& operator<<(std::ostream& os, std::unordered_map<uint16_t, int>& map) {
    for (auto it = map.begin(); it != map.end(); it++) {
        os << it->first << " " << it->second << "\n";
    }
    os << std::endl;
    return os;
}

NS_LOG_COMPONENT_DEFINE ("Spy");

NS_OBJECT_ENSURE_REGISTERED (Spy);

void CourseChangeCB (Ptr<const MobilityModel> model) {
    auto vec = model->GetPosition();
    NS_LOG_INFO ("Course Changed" << " " << vec.x << " " << vec.y << " " << vec.z );
}

TypeId
Spy::GetTypeId(void) {
    static TypeId tid = TypeId("ns3::Spy")
        .SetParent<Application> ()
        .AddConstructor<Spy> ()
    ;
    return tid;
}

Spy::Spy()
    : writeToFile(false),
    rsocket(0) {
  NS_LOG_FUNCTION(this);
}

Spy::Spy(bool writeToFile)
    : writeToFile(writeToFile),
    rsocket(0) {
  NS_LOG_FUNCTION(this);
  if (writeToFile) {
      ofSinr = std::ofstream ("DL-SINR.csv");
      ofSinr << "Time stamp,IMSI,Cell ID,RNTI,RSRP,SINR,CQI,Attached BS's Assign Number" << endl;
  }
}

Spy::Spy(RealSocket* rs)
    : writeToFile(false),
    rsocket(0) {
  NS_LOG_FUNCTION(this);
  rsocket->startSocket();
}

Spy::~Spy() {
  NS_LOG_FUNCTION(this);
  rsocket->stopTh();
}

void Spy::StartApplication () {
    NS_LOG_FUNCTION(this);
    ScheduleStartEvent();
};

void Spy::ScheduleStartEvent() {
    NS_LOG_FUNCTION(this);
    Time interval = Seconds(0.0);
    NS_LOG_LOGIC ("start at " << interval.As (Time::S));
    m_startEvent = Simulator::Schedule (interval, &Spy::StartReadingRealSocket, this);
}

void Spy::StartReadingRealSocket() {
    NS_LOG_FUNCTION(this);
    m_lastReadingTime = Simulator::Now();
    ScheduleNextRead();
}

void Spy::ScheduleNextRead() {
    Time interval = MilliSeconds(10);
    m_readEvent = Simulator::Schedule(interval, &Spy::ReadRealSocket, this);
}

void Spy::ReadRealSocket() {
    NS_LOG_FUNCTION(this);
    uint8_t* inQ_Head;
    if ((inQ_Head = rsocket->popInQ()) != nullptr) {
        auto roc_info = GetMessage(inQ_Head);
        auto roc_type = roc_info->data_type();
        Time t ("0s");
        switch(roc_type) {
            case MessageType_CreationReq: ScheduleCreation(t, roc_info, inQ_Head);
                                 break;
            case MessageType_ActionReq: ScheduleActionNow(roc_info, inQ_Head);
                                 break;
            case MessageType_DeletionReq: ScheduleDeletion(t, roc_info, inQ_Head);
                                 break;
            case MessageType_SINRReq: ScheduleSINRReqNow(roc_info, inQ_Head);
                                  break;
            case MessageType_ThroughputReq: ScheduleThroughputReq(t, roc_info, inQ_Head);
                                        break;
            case MessageType_EnbReq: ScheduleEnbReq(t, roc_info, inQ_Head);
                                 break;
            case MessageType_UeReq: ScheduleUeReq(t, roc_info, inQ_Head);
                                 break;
            default: break;
        }
        inQ_Head = nullptr;
    }
    m_lastReadingTime = Simulator::Now();
    ScheduleNextRead();
}

void Spy::StopApplication (void) {
    NS_LOG_FUNCTION(this);
    Simulator::Cancel(m_startEvent);
    Simulator::Cancel(m_readEvent);
}

void Spy::ScheduleUeReq (Time, const ROC::Message* message, uint8_t* roc) {
  NS_LOG_FUNCTION(this);
  Simulator::ScheduleNow (&Spy::roc_UeReq_CB, this, message, roc);
}

void Spy::roc_UeReq_CB (const ROC::Message* message, uint8_t* roc) {
  NS_LOG_FUNCTION(this);
  std::cout << "[Ue Req] Begin" << std::endl;
  auto ue_req = static_cast<const UeReq*> (message->data());
  auto ue_id = ue_req->ue_id()->str();
  uint32_t id = static_cast<uint32_t>(std::stoi(ue_id));
  NS_LOG_INFO("UE ID: " << id);

  UeEntry& ueEntry = manager->getUeEntry(manager->repastIDToUeEntry.at(ue_id));
  try {
    std::pair<uint8_t*, int> package = rocBuilder.buildUeResp(
      ue_id,
      std::to_string(manager->enbCellIdToAssignNumber.at(ueEntry.cellId))
    );
    rsocket->Write(package.first, package.second);
  } catch (const std::out_of_range &e) {
    std::pair<uint8_t*, int> package = rocBuilder.buildUeResp(
      ue_id,
      "-1"
    );
    rsocket->Write(package.first, package.second);
  }

  std::cout << "[Ue Req]"
    << " Repast ID" << ue_id
    << " Cell ID " << ueEntry.cellId
    << " Distance " << ueEntry.distance
    << " SINR " << ueEntry.sinr << std::endl;
  delete[] roc;
  std::cout << "[Ue Req] End" << std::endl;
}


void Spy::ScheduleEnbReq (Time t, const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    Simulator::ScheduleNow (&Spy::roc_EnbReq_CB, this, message, roc);
}

void Spy::roc_EnbReq_CB (const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    std::cout << "[Enb Req] Begin" << std::endl;
    auto eNB_req = static_cast<const EnbReq*> (message->data());
    auto eNB_id = eNB_req->enb_id()->str();
    uint32_t id = static_cast<uint32_t>(std::stoi(eNB_id));

    EnbEntry& eNBEntry = manager->getEnbEntry(manager->assignNumberToEnbEntry.at(id));
    std::pair<uint8_t*, int> package = rocBuilder.buildEnbResp(
        eNB_id,
        std::to_string(eNBEntry.usedRB),
        std::to_string(eNBEntry.number_of_ue));
    rsocket->Write(package.first, package.second);
    cout << "[Enb Req] id = "
      << id << " number of ue = "
      << eNBEntry.number_of_ue << endl;
    delete[] roc;
    std::cout << "[Enb Req] End" << std::endl;
}

void Spy::ScheduleThroughputReq (Time t, const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    Simulator::ScheduleNow (&Spy::roc_ThroughputReq_CB, this, message, roc);
}

void Spy::roc_ThroughputReq_CB (const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    std::cout << "[Throughput Req] Begin" << std::endl;
    auto thrpt_req = static_cast<const ThroughputReq*> (message->data());
    auto uav_id = thrpt_req->uav_id()->str();

    UeEntry& ueEntry = manager->getUeEntry(manager->repastIDToUeEntry.at(uav_id));
    std::stringstream ss;
    ss << ueEntry.timeStamp.As(Time::MS);

    std::pair<uint8_t*, int> package = rocBuilder.buildThroughputResp(
        uav_id,
        std::to_string(ueEntry.txThrpt) + "," + std::to_string(ueEntry.rxThrpt),
        ss.str(),
        std::to_string(ueEntry.totalBytes),
        std::to_string(ueEntry.isNoConnection));
    rsocket->Write(package.first, package.second);

    delete [] roc;
    std::cout << "[Throughput Req] End" << std::endl;
}
/**
 * Trace source
 * Update ue entry when Tx throughput updated
 * Attach timing: Configuration time
 */
void Spy::ClientTxIMSITrace (Ptr<const Packet> pkt, Time timegap, uint32_t ByteSum, uint32_t tx_id, uint32_t rx_id) {
    NS_LOG_FUNCTION(this);
    try {
        UeEntry& ueEntry = manager->getUeEntry(manager->ueIMSIToUeEntry.at(tx_id));
        ueEntry.txThrpt = ByteSum * 8.0 / timegap.GetSeconds () / 1000 / 1000;
        //NS_LOG_INFO("In duration of "<< timegap
        //    << " client IMSI: " << tx_id
        //    << " sent "<<ByteSum<<" Bytes"
        //    << " to server IMSI: " << rx_id
        //    << "Throughput: " << ByteSum * 8.0 / timegap.GetSeconds () / 1000 / 1000
        //    << " Mbps");
    } catch (std::out_of_range& e) {
        //NS_LOG_WARN("Can not locate imsi = " << tx_id <<  " ue, pass this Tx update.");
    }
}
/**
 * Trace source
 * Update ue entry when Rx throughput updated
 * Attach timing: Configuration time
 */
void Spy::ClientRxIMSITrace(Ptr<const Packet> pkt, Time timegap, uint32_t ByteSum, uint32_t tx_id, uint32_t rx_id){
    NS_LOG_FUNCTION(this);
    try {
        UeEntry& UeEntry = manager->getUeEntry(manager->ueIMSIToUeEntry.at(rx_id));
        UeEntry.rxThrpt = ByteSum * 8.0 / timegap.GetSeconds () / 1000 / 1000;
        UeEntry.timeStamp = Simulator::Now();
        UeEntry.totalBytes = pkt->GetSize();
        //NS_LOG_INFO("In duration of "<< timegap
        //    << " client IMSI: " << rx_id
        //    <<" received "<<ByteSum<<" Bytes"
        //    << " from server IMSI: " << tx_id
        //    <<"Throughput: " << ByteSum * 8.0 / timegap.GetSeconds () / 1000 / 1000
        //    << " Mbps");
    } catch (std::out_of_range& e) {
        //NS_LOG_WARN("Can not locate imsi = " << rx_id <<  " ue, pass this Rx update.");
    }
}

void Spy::ScheduleSINRReqNow (const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    Simulator::ScheduleNow (&Spy::roc_SINRReq_CB, this, message, roc);
}

void Spy::ScheduleSINRReq (Time t, const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    Simulator::Schedule (t, &Spy::roc_SINRReq_CB, this, message, roc);
}

void Spy::roc_SINRReq_CB(const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION (this);
    std::cout << "[SINR Req] Begin" << std::endl;
    auto sinrreq = static_cast<const SINRReq*> (message->data());
    auto uav_id = sinrreq->uav_id()->str();

    UeEntry& ueEntry = manager->getUeEntry(manager->repastIDToUeEntry.at(uav_id));
    cout << "[SINR Req]"
      << "Enb ID = " << ueEntry.cellId
      << " rsrp = " << ueEntry.rsrp
      << " sinr = " << ueEntry.sinr
      << " UAV ID = " << uav_id
      << " distance = " << ueEntry.distance
      << " cell id = " << ueEntry.cellId
      << " CQI = " << (uint16_t) ueEntry.cqi << endl;
    std::cout << "Rui 1 " << ueEntry.cellId << std::endl;

    // pack sinr
    if (ueEntry.cellId < 0 || ueEntry.cellId > enbNodes->GetN()) {
      ScheduleSINRReq(Time("1ms"), message, roc);
      return;
    }
    std::pair<uint8_t*, int> package = rocBuilder.buildSINRResp(
        uav_id,
        std::to_string(ueEntry.sinr),
        std::to_string(ueEntry.distance),
        std::to_string(ueEntry.cqi),
        std::to_string(manager->enbCellIdToAssignNumber.at(ueEntry.cellId)));

    std::cout << "Rui 2" << std::endl;
    // send back
    rsocket->Write(package.first, package.second);

    NS_LOG_INFO ("Enb ID = " << ueEntry.cellId
        << " rsrp = " << ueEntry.rsrp
        << " sinr = " << ueEntry.sinr
        << " UAV ID = " << uav_id
        << " distance = " << ueEntry.distance
        << " cell id = " << ueEntry.cellId
        << " CQI = " << (uint16_t) ueEntry.cqi);

    delete [] roc;
    std::cout << "[SINR Req] End" << std::endl;
}

void Spy::ScheduleCreation (Time t ,const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    Simulator::ScheduleNow (&Spy::roc_creation_CB, this, message, roc);
}

void Spy::roc_creation_CB(const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    std::cout << "[Creation] Begin" << std::endl;
    auto creationReq = static_cast<const CreationReq*>(message->data());
    auto uav_id = creationReq->uav_id()->str();
    auto lat = creationReq->latitude()->str();
    auto lng = creationReq->longitude()->str();

    if (manager->repastIDToUeEntry.count(uav_id)) return;

    // Get next available ue
    Ptr<Node> ue = manager->addUeEntry(uav_id);
    uint32_t nodeId = ue->GetId();

    //Set ue Apps start
    // ApplicationContainer apps = manager->ueNodeId2DlApps[nodeId];
    // for (uint32_t i = 0; i < apps.GetN(); i++) {
      // cout << "app" << endl;
      // Ptr<Application> app = apps.Get(i);
      // app->SetStartTime (MilliSeconds(1));
    // }

    // Put ue to position specified by lat/lng
    Ptr<WaypointMobilityModel> ueMob = ue->GetObject<WaypointMobilityModel> ();
    Vector position = GeographicPositions::GeographicToCartesianCoordinates (
            std::stod (lat),
            std::stod (lng),
            100.0,
            GeographicPositions::SPHERE);
    ueMob->AddWaypoint (Waypoint (Simulator::Now(), position));

    // Attach to closest enb
    if (!manager->isUeNodeIdAttached.count(nodeId)) {
      lteHelper->AttachToClosestEnb (ue->GetDevice(0), enbLteDevs);
      cout << "Rui: Attched!" << endl;
      manager->isUeNodeIdAttached.insert(nodeId);
    }
    uint64_t imsi = ue->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi();
    manager->isUeImsiActivated.insert(imsi);

    std::string sNodeId = std::to_string(nodeId);
    //* Update UeEntry.sinr, UeEntry.cqi, UeEntry.rsrp
    Config::ConnectWithoutContext ("/NodeList/" + sNodeId + "/DeviceList/*/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
      MakeCallback(&Spy::RsrpSinrTracedCallback, this));

    // Add traced sinks of course change to this ue
    //ueMob->TraceConnectWithoutContext ("CourseChange", MakeCallback (&CourseChangeCB));

    NS_LOG_INFO("Lauching Ue from pool:  "
        << "id=" << uav_id
        << " lat=" << lat
        << " lng=" << lng);
    std::cout << "[Creation Req]"
        << Simulator::Now()
        << "id=" << uav_id
        << " lat=" << lat
        << " lng=" << lng << std::endl;
    delete [] roc;
    std::cout << "[Creation] End" << std::endl;
}

void Spy::ScheduleAction (Time t ,const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    Simulator::Schedule (t, &Spy::roc_action_CB, this, message, roc);
}

void Spy::ScheduleActionNow (const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    Simulator::ScheduleNow (&Spy::roc_action_CB, this, message, roc);
}

void Spy::roc_action_CB(const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    std::cout << "[Action] Begin" << std::endl;

    auto actionInfoRecieved = static_cast<const ROC::ActionReq*>(message->data());
    auto uav_id = actionInfoRecieved->uav_id()->str();
    auto lat = actionInfoRecieved->latitude()->str();
    auto lng = actionInfoRecieved->longitude()->str();
    std::cout << "[ActionReq]"
        << "id=" << uav_id
        << " lat=" << lat
        << " lng=" << lng << std::endl;
    UeEntry& ueEntry = manager->getUeEntry(manager->repastIDToUeEntry.at(uav_id));
    Ptr<Node> ue = ueEntry.ue;

    std::cout << "[Action] at 1" << ueEntry.cellId <<std::endl;

    Ptr<WaypointMobilityModel> ueMob = ue->GetObject<WaypointMobilityModel> ();
    Vector position = GeographicPositions::GeographicToCartesianCoordinates (
            std::stod (lat),
            std::stod (lng),
            100.0,
            GeographicPositions::SPHERE);
    ueMob->AddWaypoint (Waypoint (Simulator::Now(), position));

    if (ueEntry.cellId < 0 || ueEntry.cellId > enbNodes->GetN()) {
      // ScheduleAction(Time("1ms"), message, roc);
      return;
    }

    EnbEntry& enbEntry = manager->getEnbEntry(manager->enbCellIdToEnbEntry
      .at(ueEntry.cellId));
    Ptr<MobilityModel> enbMob = enbEntry.enb->GetObject<MobilityModel>();
    const auto enbPosition = enbMob->GetPosition();
    ueEntry.distance = CalculateDistance(position, enbPosition);
    std::cout << "[Action] at 2" << std::endl;

    NS_LOG_INFO(""
        << "id=" << uav_id
        << " lat=" << lat
        << " lng=" << lng);

    delete [] roc;
    std::cout << "[Action] End" << std::endl;
}

void Spy::ScheduleDeletion (Time t ,const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    Simulator::ScheduleNow (&Spy::roc_deletion_CB, this, message, roc);
}

void Spy::roc_deletion_CB(const ROC::Message* message, uint8_t* roc) {
    NS_LOG_FUNCTION(this);
    std::cout << "[Deletion] Begin" << std::endl;
    auto deletion_info = static_cast<const DeletionReq*>(message->data());
    auto uav_id = deletion_info->uav_id()->str();
    cout << "[Deletion Req]"
      << " id " << uav_id << endl;
    UeEntry& ueEntry = manager->getUeEntry(manager->repastIDToUeEntry.at(uav_id));
    Ptr<Node> ue = ueEntry.ue;

    // stop all application on that ue
    // for (uint32_t i = 0; i < ue->GetNApplications (); i++)
    // {
    //   Ptr<Application> app = ue->GetApplication (i);
    //   app->SetStopTime (Seconds (0.0));
    // }

    // put it back into lauch area
    // Ptr<WaypointMobilityModel> ueMob = ue->GetObject<WaypointMobilityModel> ();
    // Vector position = GeographicPositions::GeographicToCartesianCoordinates (
    //         0.0,
    //         0.0,
    //         100.0,
    //         GeographicPositions::SPHERE);
    // ueMob->AddWaypoint (Waypoint (Simulator::Now(), position));

    // delete from manager
    manager->pushBackToAvailableQ(manager->removeUeEntry(uav_id));

    uint32_t nodeId = ue->GetId();
    // ApplicationContainer apps = manager->ueNodeId2DlApps[nodeId];
    // for (uint32_t i = 0; i < apps.GetN(); i++) {
    //   Ptr<Application> app = apps.Get(i);
    //   app->SetStopTime (MilliSeconds(1));
    // }

    std::string sNodeId = std::to_string(nodeId);
    //* Update UeEntry.sinr, UeEntry.cqi, UeEntry.rsrp
    Config::DisconnectWithoutContext ("/NodeList/" + sNodeId + "/DeviceList/*/$ns3::LteUeNetDevice/ComponentCarrierMapUe/*/LteUePhy/ReportCurrentCellRsrpSinr",
      MakeCallback(&Spy::RsrpSinrTracedCallback, this));

    uint64_t imsi = ue->GetDevice(0)->GetObject<LteUeNetDevice>()->GetImsi();
    manager->isUeImsiActivated.erase(imsi);

    NS_LOG_INFO(""
        << "id=" << uav_id);

    delete [] roc;
    std::cout << "[Deletion] End" << std::endl;
}

void Spy::setRealSocket(RealSocket* rs) {
    rsocket = rs;
}

void Spy::setUeNodes (NodeContainer* ues) {
    ueNodes = ues;
}

void Spy::setEnbNodes (NodeContainer* enbs) {
    enbNodes = enbs;
};

void Spy::setLteHelper (Ptr<LteHelper>& lh) {
    lteHelper = lh;
}

void Spy::setUeLteDevs (NetDeviceContainer& ueDevs) {
    ueLteDevs = ueDevs;
}

void Spy::setEnbLteDevs (NetDeviceContainer& enbDevs) {
    enbLteDevs = enbDevs;
}

void Spy::setManager (Manager* m) {
    manager = m;
}

void Spy::setApplicationInstaller(IApplicationInstaller &appInstaller) {
  this->appInstaller = &appInstaller;
}

/**
 * Trace source
 * Update ue entry when sinr and cqi updated
 * Attach timing: Configuration time
 */
void Spy::RsrpSinrTracedCallback (uint16_t cellId, uint16_t rnti, double rsrp, double sinr, uint8_t cqi, uint8_t componentCarrierId)  {
    NS_LOG_FUNCTION(this);
    //NS_LOG_INFO("cell id = " << cellId << " rnti = " << rnti << " rsrp = " << rsrp << " sinr = " << sinr << " cqi = " << (uint16_t) cqi);
    try {
        UeEntry& ueEntry = manager->getUeEntry(manager->ueRNTIToUeEntry.at(manager->combineCellIdRNTI(cellId, rnti)));
        ueEntry.cqi = cqi;
        ueEntry.sinr = sinr;
        ueEntry.rsrp = rsrp;
        ofSinr.precision(17);
        ofSinr << Simulator::Now() << "," << "," << cellId << "," << rnti << ","
          << rsrp << "," << sinr << "," << (uint32_t) cqi << ","
          << manager->enbCellIdToAssignNumber.at(cellId) << endl;
    } catch (std::out_of_range& e) {
        //NS_LOG_WARN("Can not locate cell id = " << cellId << " rnti = " << rnti << " ue, pass this sinr update.");
    }
}

/**
 * Trace source
 * Update ue entry when handover started
 * Attach timing: Configuration time
 */
void Spy::NotifyHandoverStartUe (std::string context, uint64_t imsi, uint16_t cellId, uint16_t rnti,
                       uint16_t targetCellId) {
    NS_LOG_FUNCTION(this);
    NS_LOG_INFO(" UE IMSI " << imsi
        << ": previously connected to CellId " << cellId << " with RNTI " << rnti
        << ", doing handover to CellId " << targetCellId);
}

/**
 * Trace source
 * Update ue entry when handover successfully end
 * Attach timing: Configuration time
 */
void Spy::NotifyHandoverEndOkUe (std::string context, uint64_t imsi, uint16_t cellId, uint16_t rnti)
{
    NS_LOG_INFO(" UE IMSI " << imsi
        << ": successful handover to CellId " << cellId << " with RNTI " << rnti
        << "Updating Ue entry");
    try{
        UeEntry& ueEntry = manager->getUeEntry(manager->ueIMSIToUeEntry.at(imsi));
        cout << "[NotifyHandoverEndOkUe]"
         << " UE IMSI " << imsi << " current Cell ID " << ueEntry.cellId
         << ": successful handover to CellId " << cellId << " with RNTI " << rnti
         << "Updating Ue entry" << endl;
        ueEntry.cellId = cellId;
        manager->ueRNTIToUeEntry.emplace(manager->combineCellIdRNTI(cellId, rnti), manager->ueIMSIToUeEntry.at(imsi));
    } catch (std::out_of_range& e) {
        NS_LOG_INFO("Failed update: can not found ue by IMSI=" << imsi << " printing map of imsi to idx of ue entry");
        std::cout << manager->ueIMSIToUeEntry;
        std::cout << manager->enbCellIdToEnbEntry;
    }
    NS_LOG_INFO("Successfully update ue entry");
}

/**
 * Trace source
 * Update ue entry when connected to a base station
 * Attach timing: Configuration time
 */
void Spy::NotifyConnectionEstablishedUe (std::string context,
                               uint64_t imsi,
                               uint16_t cellid,
                               uint16_t rnti) {
    NS_LOG_FUNCTION(this);
    NS_LOG_INFO("Connection established "
        << " UE IMSI " << imsi
        << ": connected to CellId " << cellid
        << " with RNTI " << rnti
        << "Updateing ue entry");
    cout << "[NotifyConnectionEstablishedUe]"
      << "Connection established "
      << " UE IMSI " << imsi
      << ": connected to CellId " << cellid
      << " with RNTI " << rnti
      << "Updateing ue entry" << endl;
    try{
        UeEntry& ueEntry = manager->getUeEntry(manager->ueIMSIToUeEntry.at(imsi));
        ueEntry.cellId = cellid;
        manager->ueRNTIToUeEntry.emplace(manager->combineCellIdRNTI(cellid, rnti), manager->ueIMSIToUeEntry.at(imsi));
    } catch (std::out_of_range& e) {
        NS_LOG_INFO("Failed update: can not found ue by IMSI=" << imsi << " printing map of imsi to idx of ue entry");
        std::cout << manager->ueIMSIToUeEntry;
        std::cout << manager->enbCellIdToEnbEntry;
    }
    NS_LOG_INFO("Successfully update ue entry");
}

/// Map each of UE RRC states to its string representation.
static const std::string g_ueRrcStateName[LteUeRrc::NUM_STATES] =
{
  "IDLE_START",
  "IDLE_CELL_SEARCH",
  "IDLE_WAIT_MIB_SIB1",
  "IDLE_WAIT_MIB",
  "IDLE_WAIT_SIB1",
  "IDLE_CAMPED_NORMALLY",
  "IDLE_WAIT_SIB2",
  "IDLE_RANDOM_ACCESS"
  "IDLE_CONNECTING",
  "CONNECTED_NORMALLY",
  "CONNECTED_HANDOVER",
  "CONNECTED_PHY_PROBLEM",
  "CONNECTED_REESTABLISHING"
};

/**
 * \param s The UE RRC state.
 * \return The string representation of the given state.
 */
//static const std::string & ToString (LteUeRrc::State s)
//{
//  return g_ueRrcStateName[s];
//}

void Spy::UeStateTransition (uint64_t imsi,
                            uint16_t cellId,
                            uint16_t rnti,
                            LteUeRrc::State oldState,
                            LteUeRrc::State newState) {
    //NS_LOG_INFO(" UE with IMSI " << imsi
    //    << " RNTI " << rnti
    //    << " connected to cell " << cellId
    //    << " transitions from " << ToString (oldState)
    //    << " to " << ToString (newState));
    try{
        UeEntry& ueEntry = manager->getUeEntry(manager->ueIMSIToUeEntry.at(imsi));
        if (newState >= 9 && newState < LteUeRrc::NUM_STATES) {
            ueEntry.isNoConnection = false;
        }
        else {
            ueEntry.isNoConnection = true;
        }
    } catch (std::out_of_range& e) {
        NS_LOG_INFO("Failed update: can not found ue by IMSI=" << imsi << " printing map of imsi to idx of ue entry");
        std::cout << manager->ueIMSIToUeEntry;
    }
    NS_LOG_INFO("Successfully update ue entry");

}

void Spy::RbPowerAllocationTraceSink (std::string context, uint16_t cellId, std::map<int, double> dlPowerAllocation) {
    int RbUsed = 0;
    for (auto it = dlPowerAllocation.begin(); it != dlPowerAllocation.end(); ++it){
        if (it->second > 0.1)
            RbUsed++;
    }
    //NS_LOG_INFO(" CellId : " << cellId
    //    << " The # of RBs used : " << RbUsed);
    try{
        EnbEntry& enbEntry = manager->getEnbEntry(manager->enbCellIdToEnbEntry.at(cellId));
        enbEntry.usedRB = RbUsed;
    } catch (std::out_of_range& e) {
        NS_LOG_INFO("Failed update: can not found eNB by Cell ID = " << cellId);
    }
    //NS_LOG_INFO("Successfully update eNB entry");
//   for (auto it = dlPowerAllocation.begin(); it != dlPowerAllocation.end(); ++it)
//   {
//     std::cout << "\n"
//               << " RbID : " << it->first
//               << " Power (dBm) : " << it->second;
//   }
//   std::cout << std::endl;

}

void Spy::NotifyConnectionEstablishedEnb (uint64_t imsi, uint16_t cellid,
  uint16_t rnti) {
  // if (!manager->isUeImsiActivated.count(imsi))
    // return;
  try{
    EnbEntry& enbEntry = manager->getEnbEntry(manager->enbCellIdToEnbEntry.at(cellid));
    enbEntry.number_of_ue = enbEntry.number_of_ue + 1;
    cout << "[NotifyConnectionEstablishedEnb] cell id = " << cellid
      << " number of ue " << enbEntry.number_of_ue << endl;
  } catch (std::out_of_range& e) {
      NS_LOG_INFO("Failed update: can not found eNB by Cell ID = " << cellid);
  }
}

void Spy::NotifyHandoverEndOkEnb(const uint64_t imsi,
  const uint16_t cellId, const uint16_t rnti) {
  if (!manager->isUeImsiActivated.count(imsi))
    return;
  try{
    EnbEntry& enbEntry = manager->getEnbEntry(manager->enbCellIdToEnbEntry.at(cellId));
    enbEntry.number_of_ue++;
    cout << "[NotifyHandoverEndOkEnb] cell id = " << cellId
      << " imsi = " << imsi << endl;
  } catch (std::out_of_range& e) {
      NS_LOG_INFO("Failed update: can not found eNB by Cell ID = " << cellId);
  }
}

void Spy::NotifyHandoverStartEnb(const uint64_t imsi,
  const uint16_t cellId, const uint16_t rnti, uint16_t targetCellId) {
  if (!manager->isUeImsiActivated.count(imsi))
    return;
  try{
    EnbEntry& enbEntry = manager->getEnbEntry(manager->enbCellIdToEnbEntry.at(cellId));
    enbEntry.number_of_ue--;
    cout << "[NotifyHandoverStartEnb] cell id = " << cellId
      << " imsi = " << imsi << endl;
  } catch (std::out_of_range& e) {
      NS_LOG_INFO("Failed update: can not found eNB by Cell ID = " << cellId);
  }
}

} /* namespace ns3 */

