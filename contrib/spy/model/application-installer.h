#ifndef IAPPLICATIONINSTALLER_H
#define IAPPLICATIONINSTALLER_H

#include "ns3/core-module.h"
#include "ns3/internet-module.h"

class IApplicationInstaller {
  public:
    virtual ~IApplicationInstaller(){}
    virtual void install(ns3::Ptr<ns3::Node> client, ns3::Ptr<ns3::Node> server, ns3::Ipv4Address address) = 0;
};

#endif //IAPPLICATIONINSTALLER_H
