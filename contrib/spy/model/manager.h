/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef MANAGER_H
#define MANAGER_H

#include "ns3/core-module.h"
#include "ns3/lte-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include <iostream>

namespace ns3 {

class UeEntry {
public:
    Ptr<Node> ue;
    uint8_t cqi;
    double sinr;
    uint16_t cellId;
    double distance;
    double rsrp;
    double txThrpt;
    double rxThrpt;
    Time timeStamp;
    uint32_t totalBytes;
    bool isNoConnection;
public:
    UeEntry(const UeEntry&);
    UeEntry& operator=(const UeEntry&);
    UeEntry(const UeEntry&&);
    UeEntry& operator=(const UeEntry&&);
    UeEntry(const Ptr<Node>&);
};

class EnbEntry{
public:
    Ptr<Node> enb;
    int usedRB;
    int number_of_ue;
public:
    EnbEntry(const EnbEntry&);
    EnbEntry& operator=(const EnbEntry&);
    EnbEntry(const EnbEntry&&);
    EnbEntry& operator=(const EnbEntry&&);
    EnbEntry(const Ptr<Node>&);
};

class Manager {
public:
    std::unordered_map<uint32_t, ns3::ApplicationContainer> ueNodeId2DlApps;
    std::set<uint32_t> isUeNodeIdAttached;
    std::set<uint64_t> isUeImsiActivated;
private:
    std::vector<Ptr<Node>> availableQ;
    std::vector<Ptr<Node>> occupiedQ;
public:
    Ptr<Node> popBackFromAvailableQ();
    Ptr<Node> popBackFromOccupiedQ();
    void pushBackToAvailableQ(const Ptr<Node>&);
    void pushBackToOccupiedQ(const Ptr<Node>&);
    void removeFromAvailableQ(const Ptr<Node>&);
    void removeFromOccupiedQ(const Ptr<Node>&);
    Ptr<Node>& peekBackOfAvailableQ();
    Ptr<Node>& peekBackOfOccupiedQ();
private:
    std::vector<UeEntry> ueStates;
public:
    // mapping repast uav id to idx of ue entry
    std::unordered_map<std::string, int> repastIDToUeEntry;
    // mapping ue IMSI to idx of ue entry
    std::unordered_map<uint64_t, int> ueIMSIToUeEntry;
    // mapping (cellId << 16 + rnti) to idx of ue entry
    std::unordered_map<uint32_t, int> ueRNTIToUeEntry;
    Ptr<Node> addUeEntry(std::string);
    Ptr<Node> removeUeEntry(std::string);
    UeEntry& getUeEntry(int);
private:
    std::vector<EnbEntry> enbStates;
public:
    // mapping cell id to idx of enb entry
    std::unordered_map<uint16_t, int> enbCellIdToEnbEntry;
    std::unordered_map<uint32_t, int> assignNumberToEnbEntry;
    std::unordered_map<uint16_t, uint32_t> enbCellIdToAssignNumber;
    void addEnbEntry(const Ptr<Node>&, uint32_t);
    EnbEntry& getEnbEntry(int);
public:
    uint32_t combineCellIdRNTI(uint16_t, uint16_t);
// ==================================================================================================== //
public:
    Manager(const NodeContainer& ueNodes, const NodeContainer& enbNodes);
    Manager(const NodeContainer& ueNodes, const std::unordered_map<int, ns3::Ptr<Node>>&);
};

} // namespace ns3

#endif
