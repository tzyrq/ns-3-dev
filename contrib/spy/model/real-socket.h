/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Rui Zuo <ruitso2018@gmail.com>
 */

#ifndef REAL_SOCKET_H
#define REAL_SOCKET_H

#include <future>
#include <thread>
#include <chrono>
#include <mutex>
#include <deque>

#include "ns3/nstime.h"
#include "ROC_generated.h"

extern "C" {
    #include "unp.h"
}

namespace ns3 {

class RealSocket {
private:
    int listen_fd, conn_fd;
    int local_port;
    std::thread connTh;
    std::deque<uint8_t*> inQ;
    std::deque<uint8_t*> outQ;
    EventId m_event;
private:
    void roc_creation_CB(const ROC::Message*);
    void roc_action_CB(const ROC::Message*);
    void roc_deletion_CB(const ROC::Message*);
public:
    std::promise<void> endConnTh;
    RealSocket();
    RealSocket(int);
    ~RealSocket();
    void stopEvent();
    void stopTh();
    void startSocket(void);
    void startConnThread(void);
    void connRead(std::future<void>);
    uint8_t* popInQ();
    ssize_t Write(uint8_t*, int);
private:
    std::mutex m_read;
    std::mutex m_write;
};

} /* namespcae ns3 */

#endif /* REAL_SOCKET_H */

