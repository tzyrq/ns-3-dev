/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "manager.h"

namespace ns3 {

Manager::Manager (const NodeContainer &ueNodes, const NodeContainer &enbNodes)
{
  for (NodeContainer::Iterator it = ueNodes.Begin (); it != ueNodes.End (); it++)
    {
      availableQ.push_back (*it);
    }
  for (uint32_t i = 0; i < enbNodes.GetN (); i++)
    {
      addEnbEntry (enbNodes.Get (i), i + 1);
    }
  //for (NodeContainer::Iterator it = enbNodes.Begin(); it != enbNodes.End(); it++) {
  //    addEnbEntry(*it, i);
  //}
}
Manager::Manager (const NodeContainer &ueNodes, const std::unordered_map<int, ns3::Ptr<Node>> &map) {
  for (NodeContainer::Iterator it = ueNodes.Begin (); it != ueNodes.End (); it++)
    {
      availableQ.push_back (*it);
    }
  for (const auto &e : map) {
    addEnbEntry (e.second, e.first);
  }
}
Ptr<Node>
Manager::popBackFromAvailableQ ()
{
  if (availableQ.size () == 0)
    throw std::runtime_error ("Available queue is empty");
  Ptr<Node> back = availableQ.back ();
  availableQ.pop_back ();
  return back;
}
Ptr<Node>
Manager::popBackFromOccupiedQ ()
{
  if (occupiedQ.size () == 0)
    throw std::runtime_error ("Occupied queue is empty");
  Ptr<Node> back = occupiedQ.back ();
  occupiedQ.pop_back ();
  return back;
}
void
Manager::pushBackToAvailableQ (const Ptr<Node> &ue)
{
  availableQ.emplace_back (ue);
}
void
Manager::pushBackToOccupiedQ (const Ptr<Node> &ue)
{
  occupiedQ.emplace_back (ue);
}
Ptr<Node> &
Manager::peekBackOfAvailableQ ()
{
  return availableQ.back ();
}
Ptr<Node> &
Manager::peekBackOfOccupiedQ ()
{
  return occupiedQ.back ();
}
void
Manager::removeFromAvailableQ (const Ptr<Node> &ue)
{
  for (auto it = availableQ.begin (); it != availableQ.end (); it++)
    {
      if (&(*ue) == &(**it))
        {
          availableQ.erase (it);
          return;
        }
    }
  throw std::runtime_error ("No such ue in available queue");
}
void
Manager::removeFromOccupiedQ (const Ptr<Node> &ue)
{
  for (auto it = occupiedQ.begin (); it != occupiedQ.end (); it++)
    {
      if (&(*ue) == &(**it))
        {
          occupiedQ.erase (it);
          return;
        }
    }
  // throw std::runtime_error ("No such ue in occupied queue");
}
Ptr<Node>
Manager::addUeEntry (std::string ueId)
{
  Ptr<Node> ue = popBackFromAvailableQ ();
  pushBackToOccupiedQ (ue);

  Ptr<LteUeNetDevice> dev = ue->GetDevice (0)->GetObject<LteUeNetDevice> ();
  uint64_t imsi = dev->GetImsi ();

  if (!ueIMSIToUeEntry.count(imsi)) {
    ueStates.emplace_back (UeEntry (peekBackOfOccupiedQ ()));
    int idx = ueStates.size () - 1;
    repastIDToUeEntry.emplace (ueId, idx);
    ueIMSIToUeEntry.emplace (imsi, idx);
  } else {
    int idx = ueIMSIToUeEntry[imsi];
    // getUeEntry(idx).;
    repastIDToUeEntry.emplace(ueId, idx);
  }

  return ue;
}
Ptr<Node>
Manager::removeUeEntry (std::string ueId)
{
  int idx = repastIDToUeEntry.at (ueId);
  UeEntry &ueEntry = getUeEntry (idx);
  Ptr<Node> ue = ueEntry.ue;

  for (auto it = repastIDToUeEntry.begin (); it != repastIDToUeEntry.end ();)
    {
      if (it->first == ueId)
        {
          it = repastIDToUeEntry.erase (it);
          continue;
        }
      if (it->second > idx)
        it->second = it->second - 1;
      it++;
    }
  removeFromOccupiedQ (ue);
  return ue;
}
UeEntry &
Manager::getUeEntry (int idx)
{
  return ueStates.at (idx);
}
void
Manager::addEnbEntry (const Ptr<Node> &enb, uint32_t assign_number)
{
  enbStates.emplace_back (enb);

  Ptr<LteEnbNetDevice> dev = enb->GetDevice (0)->GetObject<LteEnbNetDevice> ();
  uint16_t cellId = dev->GetCellId ();
  int idx = enbStates.size () - 1;

  enbCellIdToEnbEntry.emplace (cellId, idx);
  assignNumberToEnbEntry.emplace (assign_number, idx);
  enbCellIdToAssignNumber.emplace(cellId, assign_number);
}
uint32_t
Manager::combineCellIdRNTI (uint16_t cellId, uint16_t rnti)
{
  uint32_t res = (uint32_t) cellId;
  res = (res << 16) + rnti;
  return res;
}
EnbEntry &
Manager::getEnbEntry (int idx)
{
  return enbStates.at (idx);
}

UeEntry::UeEntry (const UeEntry &ueEntry) : ue (ueEntry.ue)
{
  cqi = ueEntry.cqi;
  sinr = ueEntry.sinr;
  cellId = ueEntry.cellId;
  distance = ueEntry.distance;
  rsrp = ueEntry.rsrp;
  txThrpt = ueEntry.txThrpt;
  rxThrpt = ueEntry.rxThrpt;
  timeStamp = ueEntry.timeStamp;
  totalBytes = ueEntry.totalBytes;
  isNoConnection = ueEntry.isNoConnection;
}
UeEntry &
UeEntry::operator= (const UeEntry &ueEntry)
{
  ue = ueEntry.ue;
  cqi = ueEntry.cqi;
  sinr = ueEntry.sinr;
  cellId = ueEntry.cellId;
  distance = ueEntry.distance;
  rsrp = ueEntry.rsrp;
  txThrpt = ueEntry.txThrpt;
  rxThrpt = ueEntry.rxThrpt;
  timeStamp = ueEntry.timeStamp;
  totalBytes = ueEntry.totalBytes;
  isNoConnection = ueEntry.isNoConnection;
  return *this;
}
UeEntry::UeEntry (const UeEntry &&ueEntry) : ue (ueEntry.ue)
{
  cqi = ueEntry.cqi;
  sinr = ueEntry.sinr;
  cellId = ueEntry.cellId;
  distance = ueEntry.distance;
  rsrp = ueEntry.rsrp;
  txThrpt = ueEntry.txThrpt;
  rxThrpt = ueEntry.rxThrpt;
  timeStamp = ueEntry.timeStamp;
  totalBytes = ueEntry.totalBytes;
  isNoConnection = ueEntry.isNoConnection;
}
UeEntry &
UeEntry::operator= (const UeEntry &&ueEntry)
{
  ue = ueEntry.ue;
  cqi = ueEntry.cqi;
  sinr = ueEntry.sinr;
  cellId = ueEntry.cellId;
  distance = ueEntry.distance;
  rsrp = ueEntry.rsrp;
  txThrpt = ueEntry.txThrpt;
  rxThrpt = ueEntry.rxThrpt;
  timeStamp = ueEntry.timeStamp;
  totalBytes = ueEntry.totalBytes;
  isNoConnection = ueEntry.isNoConnection;
  return *this;
}
UeEntry::UeEntry (const Ptr<Node> &ue) : ue (ue)
{
  cqi = 0;
  sinr = -1.0;
  cellId = -1;
  distance = -1.0;
  rsrp = -1.0;
  txThrpt = -1.0;
  rxThrpt = -1.0;
  timeStamp = Seconds (0);
  totalBytes = 0;
  isNoConnection = true;
}

EnbEntry::EnbEntry (const EnbEntry &enbEntry) :
  enb (enbEntry.enb),
  usedRB (enbEntry.usedRB),
  number_of_ue(enbEntry.number_of_ue)
{
}
EnbEntry &
EnbEntry::operator= (const EnbEntry &enbEntry)
{
  enb = enbEntry.enb;
  usedRB = enbEntry.usedRB;
  number_of_ue = enbEntry.number_of_ue;
  return *this;
}
EnbEntry::EnbEntry (const EnbEntry &&enbEntry) :
  enb (enbEntry.enb),
  usedRB (enbEntry.usedRB),
  number_of_ue(enbEntry.number_of_ue)
{
}
EnbEntry &
EnbEntry::operator= (const EnbEntry &&enbEntry)
{
  enb = enbEntry.enb;
  usedRB = enbEntry.usedRB;
  number_of_ue = enbEntry.number_of_ue;
  return *this;
}
EnbEntry::EnbEntry (const Ptr<Node> &enb) : enb (enb), usedRB (0), number_of_ue(0)
{
}

} // namespace ns3
